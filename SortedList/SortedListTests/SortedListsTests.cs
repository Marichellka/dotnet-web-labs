using System;
using System.Collections.Generic;
using SortedList;
using NUnit.Framework; 

namespace SortedListTests;

public class SortedListsTests
{
    [Test]
    public void ConstructorWithEnumerable_InitializeCustomListWithEnumerable_InitializedCorrectly()
    {
        var expected = new List<int> { 1, 2, 3, 4, 5 };
        SortedList<int> list = new (expected);

        Assert.AreEqual(expected, list);
    }
    
    [Test]
    public void ConstructorWithEnumerableCollection_InitializeCustomListWithNull_ThrownArgumentNullException()
    {
        Assert.Throws(typeof(ArgumentNullException), () => new SortedList<int>(null));
    }
    
    [Test]
    public void CopyTo_CopyToSmallerArray_ValuesCopiedCorrectly()
    {
        SortedList<int> list = new() { 1, 2, 3, 4 };
        int[] array = new int[3];
        
        list.CopyTo(array, 1);
        
        Assert.AreEqual(new[] {0, 1, 2}, array);
    }
    
    [Test]
    public void CopyTo_ArrayIndexMoreThanSize_ThrownArgumentOutOfRangeException()
    {
        SortedList<int> list = new() { 1, 2, 3, 4 };
        int[] array = new int[3];
        
        Assert.Throws(typeof(ArgumentOutOfRangeException), () => list.CopyTo(array, 4));
    }
    
    [Test]
    public void CopyTo_CopyEmptySortedList_ArrayValueNotChanged()
    {
        SortedList<int> list = new SortedList<int>();
        int[] array = new int[3];
        list.CopyTo(array, 1);
        Assert.AreEqual(new[] {0, 0, 0}, array);
    }
    
    [Test]
    public void CopyTo_CopyToNullArray_ThrownArgumentNullException()
    {
        SortedList<int> list = new();
        Assert.Throws(typeof(ArgumentNullException), () => list.CopyTo(null, 4));
    }
    
    [Test]
    public void ToArray_TransformToArray_TransformedCorrectly()
    {
        SortedList<int> list = new() { 1, 2, 3, 4 };
        Assert.AreEqual(new[] {1, 2, 3, 4}, list.ToArray());
    }
    
    [Test]
    public void ToArray_TransformEmptyList_ReturnedEmptyArrayWithSize0()
    {
        SortedList<int> list = new();
        Assert.AreEqual(new int[0], list.ToArray());
    }
    
    [Test]
    public void AddTo_AddToFirst_AddedCorrectly()
    {
        SortedList<int> list = new () {10};
        list.Add(4);
        Assert.AreEqual(new[] { 4, 10}, list);
    }
    
    [Test]
    public void AddTo_AddToMiddle_AddedCorrectly()
    {
        SortedList<int> list = new () {4, 10};
        list.Add(8);
        Assert.AreEqual(new[] { 4, 8, 10}, list);
    }
    
    [Test]
    public void AddTo_AddToLast_AddedCorrectly()
    {
        SortedList<int> list = new () {4, 8, 10};
        list.Add(12);
        Assert.AreEqual(new[] { 4, 8, 10, 12}, list);
    }
    
    [Test]
    public void Remove_RemoveFirst_RemovedCorrectly()
    {
        SortedList<int> list = new () { 4, 8, 10, 12 };
        list.Remove(4);
        Assert.AreEqual(new []{8, 10, 12}, list);
    }
    
    [Test]
    public void Remove_RemoveMiddle_RemovedCorrectly()
    {
        SortedList<int> list = new () { 8, 10, 12 };
        list.Remove(10);
        Assert.AreEqual(new []{8, 12}, list);
        Assert.False(list.Remove(10));
    }
    
    [Test]
    public void Remove_RemoveLast_RemovedCorrectly()
    {
        SortedList<int> list = new () { 8, 10, 12 };
        list.Remove(12);
        Assert.AreEqual(new []{8, 10}, list);
    }
    
    [Test]
    public void Remove_RemoveNonExistentItem_ReturnedFalse()
    {
        SortedList<int> list = new () { 8, 10, 12 };
        Assert.False(list.Remove(4));
    }
    
    [Test]
    public void RemoveAt_RemoveAtFirst_RemovedCorrectly()
    {
        SortedList<int> list = new () { 4, 8, 10, 12 };
        list.RemoveAt(0);
        Assert.AreEqual(new []{8, 10, 12}, list);
    }
    
    [Test]
    public void RemoveAt_RemoveAtMiddle_RemovedCorrectly()
    {
        SortedList<int> list = new () { 8, 10, 12 };
        list.RemoveAt(1);
        Assert.AreEqual(new []{8, 12}, list);
    }
    
    [Test]
    public void RemoveAt_RemoveAtLast_RemovedCorrectly()
    {
        SortedList<int> list = new () { 8, 10, 12 };
        list.RemoveAt(2);
        Assert.AreEqual(new []{8, 10}, list);
    }
    
    [Test]
    public void RemoveAt_RemoveAtOutOfRange_ThrownArgumentOutOfRangeException()
    {
        SortedList<int> list = new () { 4, 8 };
        Assert.Throws(typeof(ArgumentOutOfRangeException), () => list.RemoveAt(2));
    }
    
    [Test]
    public void Contains_ContainsExistent_ReturnedTrue()
    {
        SortedList<string> list = new(){ "foo", "baz", "bar" };
        Assert.True(list.Contains("baz"));
    }
    
    [Test]
    public void Contains_ContainsNonExistent_ReturnedFalse()
    {
        SortedList<string> list = new(){ "foo", "baz", "bar" };
        Assert.False(list.Contains("fizz"));
    }
    
    [Test]
    public void Contains_ContainsNull_ReturnedFalse()
    {
        SortedList<string> list = new(){ "foo", "baz", "bar" };
        Assert.False(list.Contains(null));
    }
    
    [Test]
    public void Clear_ClearList_ListIsEmpty()
    {
        SortedList<string> list = new (){ "foo", "baz" };
        list.Clear();
        Assert.IsEmpty(list);
    }

    [Test]
    public void IndexOf_IndexOfFirst_Returned0()
    {
        SortedList<int> list = new() { 4, 4, 10 };
        Assert.AreEqual(0, list.IndexOf(4));
    }
    
    [Test]
    public void IndexOf_IndexOfNonExistent_ReturnedNegative1()
    {
        SortedList<int> list = new() { 4, 4, 10 };
        Assert.AreEqual(-1, list.IndexOf(8));
    }
    
    [Test]
    public void IndexOf_IndexOfNotFirst_ReturnedCorrectIndex()
    {
        SortedList<int> list = new() { 4, 4, 10 };
        Assert.AreEqual(2, list.IndexOf(10));
    }

    [Test]
    public void Indexer_GetByFirstIndex_ReturnedElement()
    {
        SortedList<int> list = new() { 1, 2, 3, 4 };
        Assert.AreEqual(1, list[0]);
    }
    
    [Test]
    public void Indexer_GetByNotFirstIndex_ReturnedElement()
    {
        SortedList<int> list = new() { 1, 2, 3, 4 };
        Assert.AreEqual(3, list[2]);
    }
    
    [Test]
    public void Indexer_GetByOutOfRangeIndex_ThrownArgumentOutOfRangeException()
    {
        SortedList<int> list = new() { 1, 2, 3, 4 };
        Assert.Throws(typeof(ArgumentOutOfRangeException), () =>
        {
            int _ = list[4];
        });
    }
}