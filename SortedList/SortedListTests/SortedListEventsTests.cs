﻿using System.Collections.Generic;
using SortedList;
using NUnit.Framework; 

namespace SortedListTests;

public class SortedListEventsTests
{
    [Test]
    public void ItemAddedEvent_ItemAddedFirst_IsFirstTrueAndElement()
    {
        SortedList<int> list = new();

        List<(bool, int)> addedItems = new();

        list.ItemAdded += (_, args) =>
        {
            addedItems.Add((args.IsHead, args.Item));
        };

        list.Add(4);
        list.Add(1);

        Assert.AreEqual(new []
        {
            (true, 4),
            (true, 1)
        }, addedItems);
    }
    
    [Test]
    public void ItemAddedEvent_ItemAddedNotFirst_IsFirstFalseAndElement()
    {
        SortedList<int> list = new(){1, 2, 4};

        List<(bool, int)> addedItems = new();

        list.ItemAdded += (_, args) =>
        {
            addedItems.Add((args.IsHead, args.Item));
        };

        list.Add(3);
        list.Add(5);

        Assert.AreEqual(new []
        {
            (false, 3),
            (false, 5)
        }, addedItems);
    }
    
    [Test]
    public void ItemRemovedEvent_ItemRemovedFirst_IsFirstTrueAndElement()
    {
        SortedList<int> list = new(){ 1, 2, 3, 4 };

        List<(bool, int)> removedItems = new();

        list.ItemRemoved += (_, args) =>
        {
            removedItems.Add((args.IsHead, args.Item));
        };

        list.Remove(1);
        list.RemoveAt(0);

        Assert.AreEqual(new[]
        {
            (true, 1),
            (true, 2),
        }, removedItems);
    }
    
    [Test]
    public void ItemRemovedEvent_ItemRemovedNotFirst_IsFirstFalseAndElement()
    {
        SortedList<int> list = new(){ 1, 2, 3, 4 };

        List<(bool, int)> removedItems = new();

        list.ItemRemoved += (_, args) =>
        {
            removedItems.Add((args.IsHead, args.Item));
        };

        list.RemoveAt(1);
        list.Remove(3);

        Assert.AreEqual(new[]
        {
            (false, 2),
            (false, 3)
        }, removedItems);
    }
    
    [Test]
    public void ClearedEvent_Cleared_ClearedTrue()
    {
        SortedList<string> deque = new(){ "foo", "baz", "bar" };

        bool clearedEventFlag = false;

        deque.Cleared += (_, _) => clearedEventFlag = true;

        deque.Clear();

        Assert.True(clearedEventFlag);
    }
}