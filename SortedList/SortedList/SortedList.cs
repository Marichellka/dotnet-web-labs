﻿using System.Collections;

namespace SortedList;

public class SortedList<T>: IReadOnlyList<T?>, IList, IList<T?>
    where T : IComparable<T>
{
    private ListNode<T?>? _head;

    public event EventHandler<ItemAddedEventArgs<T>>? ItemAdded;
    public event EventHandler<ItemRemovedEventArgs<T>>? ItemRemoved;
    public event EventHandler? Cleared;

    public int Count { get; private set; }

    bool ICollection.IsSynchronized => false;
    object ICollection.SyncRoot => null!;
    bool IList.IsFixedSize => false;
    bool IList.IsReadOnly => false; 
    bool ICollection<T?>.IsReadOnly => false; 
    
    public SortedList () { }

    public SortedList(IEnumerable<T> initialValues)
    {
        if (initialValues is null)
        {
            throw new ArgumentNullException(nameof(initialValues));
        }

        foreach (T value in initialValues)
        {
            Add(value);
        }
    }

    public void Add(T? item)
    {
        ListNode<T?> newNode = new ListNode<T?>(item);

        ListNode<T?>? previous = null;
        ListNode<T?>? current = _head;

        while (current?.Value?.CompareTo(newNode.Value) < 0)
        {
            previous = current;
            current = current.Next;
        }

        if (current?.Prev is { } prev)
        {
            newNode.Prev = prev;
            prev.Next = newNode;
        }

        if (current is { })
        {
            newNode.Next = current;
            current.Prev = newNode;
        }

        if (previous is null) // if newNode is first
        {
            _head = newNode;
        }

        if (newNode.Next is null && previous is { }) // if newNode is last
        {
            previous.Next = newNode;
            newNode.Prev = previous;
        }
        
        Count += 1;
        ItemAdded?.Invoke(this, new(item, newNode.Prev is null));
    }
    
    public int Add(object? value)
    {
        if (value is null)
            throw new ArgumentNullException(nameof(value));
        
        Add((T)value);
        return Count;
    }
    
    public bool Contains(T? item)
    {
        foreach (T? value in this)
        {
            if (EqualityComparer<T>.Default.Equals(value, item))
                return true;
        }

        return false;
    }
    
    public bool Contains(object? value) => Contains((T?)value);

    public void CopyTo(T?[] array, int arrayIndex)
    {
        if (array is null)
        {
            throw new ArgumentNullException(nameof(array));
        }
        if (arrayIndex < 0 || arrayIndex >= array.Length)
        {
            throw new ArgumentOutOfRangeException(nameof(arrayIndex));
        }
        
        ListNode<T?>? current = _head;
        while (arrayIndex < array.Length && current is not null)
        {
            array[arrayIndex] = current.Value;
            arrayIndex++;
            current = current.Next;
        }
    }
    
    public void CopyTo(Array array, int index) => CopyTo((T[])array, index);

    public void Clear()
    {
        _head = null;   
        Count = 0;
        Cleared?.Invoke(this, EventArgs.Empty);
    }
    
    public int IndexOf(T? item)
    {
        int index = 0;
            
        foreach (T? value in this)
        {
            if (EqualityComparer<T>.Default.Equals(value, item))
                return index;
            
            index++;
        }

        return -1;
    }

    public int IndexOf(object? value) => IndexOf((T?)value);

    public void Insert(int index, T? item)
    {
        throw new NotSupportedException();
    }

    public void Insert(int index, object? value) => Insert(index, (T?)value);

    public bool Remove(T? item)
    {
        ListNode<T?>? current = _head;
        
        while (current is not null)
        {
            if (EqualityComparer<T>.Default.Equals(current.Value, item))
            {
                RemoveNode(current);
                return true;
            }

            current = current.Next;
        }

        return false;
    }

    public void Remove(object? value) => Remove((T?)value);

    public void RemoveAt(int index)
    {
        RemoveNode(GetNodeByIndex(index));
    }

    private void RemoveNode(ListNode<T?> node)
    {
        Count--;
 
        if (node.Prev is not null)
        {
            node.Prev.Next = node.Next;
        }
        
        if (node.Next is not null)
        {
            node.Next.Prev = node.Prev;
        }

        if (node.Prev is null)
        {
            _head = node.Next;
        }

        ItemRemovedEventArgs<T> eventArgs = new(node.Value, node.Prev is null);
        ItemRemoved?.Invoke(this, eventArgs);
    }

    private ListNode<T?> GetNodeByIndex(int index)
    {
        if (index < 0 || index >= Count)
        {
            throw new ArgumentOutOfRangeException(nameof(index));
        }
        
        ListNode<T?>? current = _head;
        int currentIndex = 0;
        
        while (currentIndex < index)
        {
            current = current!.Next;
            currentIndex++;
        }

        return current!;
    }
    
    public IEnumerator<T?> GetEnumerator()
    {
        ListNode<T?>? currentNode = _head;

        while (currentNode is not null)
        {
            yield return currentNode.Value;
            currentNode = currentNode.Next;
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public T?[] ToArray()
    {
        T?[] array = new T?[Count];
        if (Count > 0)
        {
            CopyTo(array, 0);
        }
        return array;
    }

    public T? this[int index]
    {
        get => GetNodeByIndex(index).Value;
        set => throw new NotSupportedException();
    }

    object? IList.this[int index]
    {
        get => this[index]; 
        set => throw new NotSupportedException();
    }
}