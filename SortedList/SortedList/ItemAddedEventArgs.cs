﻿namespace SortedList;

public class ItemAddedEventArgs<T>: EventArgs
{
    public T? Item { get; }
    public bool IsHead { get; }

    public ItemAddedEventArgs(T? item, bool isHead)
    {
        Item = item;
        IsHead = isHead;
    }
}