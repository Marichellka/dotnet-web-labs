﻿namespace SortedList;

public class ItemRemovedEventArgs<T>: EventArgs
{
    public T? Item { get; }
    public bool IsHead { get; }
    
    public ItemRemovedEventArgs(T? item, bool isHead)
    {
        Item = item;
        IsHead = isHead;
    }
}