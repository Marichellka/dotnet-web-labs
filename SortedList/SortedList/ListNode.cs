﻿using System.Text;

namespace SortedList;

internal class ListNode<T>
{
    public ListNode<T?>? Next { get; set; }
    public ListNode<T?>? Prev { get; set; }
    public T? Value { get; set; }

    public ListNode(T? value)
    {
        Value = value;
    }

    public override string ToString()
    {
        StringBuilder builder = new();

        builder.Append($"[Prev: {(Prev is null ? "[null]" : Prev.Value)}]");
        builder.Append($"[Current: {Value}]");
        builder.Append($"[Next: {(Next is null ? "[null]" : Next.Value)}]");

        return builder.ToString();
    }
}