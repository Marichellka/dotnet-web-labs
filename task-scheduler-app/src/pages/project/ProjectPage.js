import React, { useState, useEffect, useContext } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faPlusSquare, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import TaskList from "../task/TaskList";
import TeamList from "../team/TeamList";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss"

function Project ({project, setMessage, setSpinner, setVisibility}) {
    const navigate = useNavigate();
	const [teams, setTeams] = useState([]);
	const [tasks, setTasks] = useState([]);
	const rootApiUrl =  useContext(UrlContext);


	useEffect(
		() =>{
			setSpinner(true);
			sendRequest(null, rootApiUrl + "projects/" + project.id + "/teams", "GET")
				.then((response) => setTeams(response))
				.catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
			
			sendRequest(null, rootApiUrl + "projects/" + project.id + "/stories", "GET")
				.then((response) => {
					setSpinner(false);
					setTasks(response)
				})
				.catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
		},[]
	);

    return (
        <div>
			<hr/>
            <div className={classes.record}> 
				<span>Description:</span> 
				<div>{project.description}</div>
				<hr/>
			</div>
			<div className={classes.header}>
				<span>Teams:</span>
				<FontAwesomeIcon icon={faPlusSquare}  className={classes.button} size="lg"
					onClick={_ => navigate("/projects/" + project.id + "/create-team")}/>
			</div>
			<TeamList teams={teams} />
			<hr/>
			<div className={classes.header}>
				<span>Tasks:</span>
				<FontAwesomeIcon icon={faPlusSquare}  className={classes.button} size="lg"
					onClick={_ => navigate("/projects/" + project.id + "/create-task")}/>
			</div>
            <TaskList tasks={tasks} />
        </div>
        
    );
};

function ProjectPage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
	let { projectId } = useParams();
    const [project, setProject] = useState();
	const rootApiUrl =  useContext(UrlContext);

    useEffect(
		() =>{
			setSpinner(true);
			sendRequest(null, rootApiUrl + "projects/" + projectId, "GET")
				.then((response) => {
					setSpinner(true);
					setProject(response)
				})
				.catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
		},[]
	);

    return (
		<div className={classes.wrapper}>
			<div className={classes.record} style={{"cursor":"pointer"}} onClick={_ => navigate("/projects/")}>
				<FontAwesomeIcon icon={faArrowLeft} className={classes.button} size="lg"/>
				<span style={{"margin":"5px"}}>All projects</span>
			</div>
			<header className={classes.header}>
				<div><span>Project: </span>{project?.name}</div>
				<div>
					<FontAwesomeIcon icon={faEdit} className={classes.button} size="lg"
						onClick={_ => navigate("/projects/" + project.id + "/update")}/>
				</div>
			</header>
			{project ? (
				<Project project={project} setMessage={setMessage} 
					setSpinner={setSpinner} setVisibility={setVisibility}/>
			) : null}
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</div>
	);
};


export default ProjectPage;