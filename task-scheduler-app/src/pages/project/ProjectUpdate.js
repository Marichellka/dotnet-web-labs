import React, {useContext, useState, useEffect} from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss"


function ProjectUpdatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    let { projectId } = useParams();
    const [project, setProject] = useState();
	const rootApiUrl =  useContext(UrlContext);

    function updateHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(project, rootApiUrl + "projects/", "PUT")
                    .then(() => navigate("/projects/"+projectId))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }

    useEffect(
		() =>{
            setSpinner(true);
            sendRequest(null, rootApiUrl + "projects/" + projectId, "GET")
				.then((project) => {
                    setSpinner(false);
                    delete project.projectId;
                    setProject(project);
                })
				.catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
		},[]
	);


    return (
		<form onSubmit={updateHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>Update project</span>
                <FontAwesomeIcon icon={faSave} onClick={updateHandler} className={classes.button} size="lg"/>
            </header>
            <div className={classes.record}>
                <span>Name: </span>
                <input placeholder="Name..." value={project?.name} required
                    onChange={event => setProject({...project, name: event.target.value})}/>
            </div>
            <div className={classes.record}>
                <span>Description: </span>
                <input placeholder="Description..." value={project?.description} 
                    onChange={event => setProject({...project, description: event.target.value})}/>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</form>
	);
};


export default ProjectUpdatePage;