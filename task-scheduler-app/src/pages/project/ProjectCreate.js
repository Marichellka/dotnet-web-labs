import React, {useContext, useState} from 'react';
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import { parseError, sendRequest } from "../../common/utils";
import classes from "../../scss/shared.module.scss"

const emptyProject = {
    name: "",
	description: "",
}

function ProjectsCreatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    const [newProject, setNewProjects] = useState(emptyProject);
	const rootApiUrl =  useContext(UrlContext);

    function createHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(newProject, rootApiUrl + "projects/", "POST")
                    .then(() => navigate("/"))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }


    return (
		<form onSubmit={createHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>New project</span>
                <FontAwesomeIcon icon={faSave} onClick={createHandler} className={classes.button} size="lg"/>
            </header>
            <div className={classes.record}>
                <span>Name: </span>
                <input placeholder="Name..." value={newProject.name} required
                    onChange={event => setNewProjects({...newProject, name: event.target.value})}/>
            </div>
			<div className={classes.record}>
                <span>Description: </span>
                <input placeholder="Description..." value={newProject.description}
                    onChange={event => setNewProjects({...newProject, description: event.target.value})}/>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
        </form>
	);


};


export default ProjectsCreatePage;