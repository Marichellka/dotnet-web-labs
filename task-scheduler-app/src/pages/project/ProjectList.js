import { Link } from "react-router-dom";
import classes from '../../scss/list.module.scss';

function ProjectList({projects}){
    return (
        <div className={classes.list}>
            {projects.map(project => {
                return (
                    <Link key={project.id} to={"/projects/" + project.id} className={classes.listItem}>
                            {project.name}
                    </Link> 
                );
            })}
        </div>
    );
};

export default ProjectList;