import { Link } from "react-router-dom";
import classes from '../../scss/list.module.scss';

function TeamList({teams}){
    return (
        <div className={classes.list}>
            {teams.map(team => {
                    return (
                        <Link key={team.id} to={"/teams/" + team.id}  className={classes.listItem}>
                            {team.name}
                        </Link>
                    );
                })}
        </div>
    );
};

export default TeamList;