import React, { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit, faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import UserList from "../user/UserList";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss"

function Team ({team, setMessage, setSpinner, setVisibility}) {
    const navigate = useNavigate();
    const [project, setProject] = useState();
	const [members, setMembers] = useState([]);
	const rootApiUrl =  useContext(UrlContext);

    useEffect(() => {
        setSpinner(true);
		sendRequest(null, rootApiUrl + "projects/" + team.projectId, "GET")
			.then((response) => setProject(response))
			.catch((error)=>{
				setMessage(parseError(error));
				setVisibility(true);
            });
		
		sendRequest(null, rootApiUrl + "teams/" + team.id + "/members", "GET")
			.then((response) => {
				setSpinner(false);
				setMembers(response)
			})
			.catch((error)=>{
				setMessage(parseError(error));
				setVisibility(true);
            });
	}, []);


    return (
        <div>
			<hr/>
			<div className={classes.record}>
				<span>Project: </span>
				<Link to={"/projects/" + team.projectId} className={classes.link}>
					{project? project.name: ""}
				</Link>
			</div>
			<div className={classes.header}>
				<span>Employees:</span>
				<FontAwesomeIcon icon={faPlusSquare}  className={classes.button}  size="lg"
					onClick={_ => navigate("/teams/" + team.id + "/create-user")}/>
			</div>
			<UserList users={members} />
        </div>
        
    );
};

function TeamPage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
	let { teamId } = useParams();
    const [team, setTeam] = useState();
	const rootApiUrl =  useContext(UrlContext);

    useEffect(() => {
        setSpinner(true);
		sendRequest(null, rootApiUrl + "teams/" + teamId, "GET")
			.then((response) => {
				setSpinner(false);
				setTeam(response)
			})
			.catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
	}, []
	);

    return (
		<div className={classes.wrapper}>
			<header className={classes.header}>
				<div>
					<span>Team: </span>{team?.name}
				</div>
				<div>
					<FontAwesomeIcon icon={faEdit} className={classes.button} size="lg"
						onClick={_ => navigate("/teams/" + team.id + "/update")} />
					<FontAwesomeIcon icon={faTrashAlt} className={classes.button}  size="lg"
						onClick={ (event) => {
							event.preventDefault();
							sendRequest(null, rootApiUrl + "teams/" + team.id, "DELETE")
								.then(()=>navigate("/projects/"+team.projectId))
								.catch((error)=>{
									setMessage(parseError(error));
									setVisibility(true);
								});
						}}/>
				</div>
			</header>
			{team ? (
				<Team team={team} setMessage={setMessage} 
					setSpinner={setSpinner} setVisibility={setVisibility}/>
			) : null}
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</div>
	);
};


export default TeamPage;