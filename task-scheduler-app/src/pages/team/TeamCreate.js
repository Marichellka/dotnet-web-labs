import React, {useContext, useState, useEffect} from 'react';
import { Link, useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss";

const emptyTeam = {
    name: "",
}

function TeamsCreatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    let { projectId } = useParams();
    const [newTeam, setNewTeams] = useState({...emptyTeam, projectId:projectId});
	const rootApiUrl =  useContext(UrlContext);

    function createHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(newTeam, rootApiUrl + "teams/", "POST")
                    .then(() => navigate("/projects/"+projectId))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }


    return (
		<form onSubmit={createHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>New team</span>
                <FontAwesomeIcon icon={faSave} onClick={createHandler}  className={classes.button} size="lg"/>
            </header>
            <div className={classes.record}>
                <span>Name: </span>
                <input placeholder="Name..." value={newTeam.name} required
                    onChange={event => setNewTeams({...newTeam, name: event.target.value})}/>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</form>
	);
};


export default TeamsCreatePage;