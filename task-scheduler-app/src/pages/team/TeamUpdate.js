import React, {useContext, useState, useEffect} from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss";

function TeamUpdatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    let { teamId } = useParams();
    const [team, setTeam] = useState();
	const rootApiUrl =  useContext(UrlContext);

    function updateHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(team, rootApiUrl + "teams/", "PUT")
                    .then(() => navigate("/teams/"+teamId))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }

    useEffect(
		() =>{
            setSpinner(true);
            sendRequest(null, rootApiUrl + "teams/" + teamId, "GET")
				.then((team) => {
                    setSpinner(false);
                    delete team.projectId;
                    setTeam(team);
                })
				.catch((error)=>{
                    setMessage(parseError(error));
                    setVisibility(true);
                    });
		},[]
	);


    return (
		<form onSubmit={updateHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>Update team</span>
                <FontAwesomeIcon icon={faSave} onClick={updateHandler} size="lg" className={classes.button}/>
            </header>
            <div className={classes.record}>
                <span>Name: </span>
                <input placeholder="Name..." value={team?.name} required
                    onChange={event => setTeam({...team, name: event.target.value})}/>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</form>
	);
};


export default TeamUpdatePage;