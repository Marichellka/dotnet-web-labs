import React, { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit, faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import ProjectList from "../project/ProjectList";
import classes from "../../scss/shared.module.scss"

function HomePage(){
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    const [projects, setProjects] = useState([]);
	const rootApiUrl =  useContext(UrlContext);

    useEffect(
		() =>{
			setSpinner(true);
			sendRequest(null, rootApiUrl + "projects/", "GET")
				.then((response) => {
					setProjects(response);
					setSpinner(false);
				})
				.catch((error)=>{
					setMessage(parseError(error));
					setVisibility(true);
				});
		},[]
	);

    return(
		<div className={classes.wrapper}>
			<header className={classes.header}>
				<span>Projects:</span>
				<FontAwesomeIcon className={classes.button} icon={faPlusSquare}  
					onClick={_ => navigate("/projects/create")}  size="lg"/>
			</header>
            <ProjectList projects={projects} />
			<Message visibility={messageVisibility} setVisibility={setVisibility} message={message}/>
			<Spinner visibility={spinnerVisibility} />
		</div>
    )
};

export default HomePage;