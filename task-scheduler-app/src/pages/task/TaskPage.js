import React, {useContext, useState, useEffect} from 'react';
import { Link, useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { Status, Priority, getKeyByValue } from './TaskEnums';
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss"

function Task ({task, setMessage, setSpinner, setVisibility}) {
    const [user, setUser] = useState();
	const [project, setProject] = useState();
	const rootApiUrl =  useContext(UrlContext);

    useEffect(() => {
        setSpinner(true);
		if (task.userId!=null){
			sendRequest(null, rootApiUrl + "users/" + task.userId, "GET")
			.then((response) => setUser(response))
			.catch((error)=>{
				try {
					error = JSON.parse(error.message);
					setMessage(error.message);
				} catch (_){
					setMessage(error)
				}
				setVisibility(true);
			});
		}
	
		sendRequest(null, rootApiUrl + "projects/" + task.projectId, "GET")
			.then((response) => {
				setSpinner(false);
				setProject(response)
			})
			.catch((error)=>{
                try {
					error = JSON.parse(error.message);
					setMessage(error.message);
				} catch (_){
					setMessage(error)
				}
                setVisibility(true);
            });
	}, []);

    return (
        <div>
			<hr/>
			<div className={classes.record}>
				<span>Description:</span> 
				<div>{task.description}</div>
			</div>
			<div className={classes.record}>
				<span>Assigned to: </span>
				<Link to={user? "/users/" + task.userId: "#"} className={classes.link}>
					{user? user.fullName : "Unassigned"}
				</Link>
			</div>
			<div className={classes.record}>
				<span>Project: </span>
				<Link to={"/projects/" + task.projectId} className={classes.link}>
					{project? project.name : ""}
				</Link>
			</div>
			<div className={classes.record}>
				<span> Status: </span>
				{getKeyByValue(Status, task.status)}
			</div>
			<div className={classes.record}>
				<span> Priority: </span>
				{getKeyByValue(Priority, task.priority)}
			</div>
			<div className={classes.record}>
				<span> Story points: </span>
				{task.storyPoints}
			</div>
		</div>
    );
};

function TaskPage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
	let { taskId } = useParams();
    const [task, setTask] = useState();
	const rootApiUrl =  useContext(UrlContext);

    useEffect(
		() =>{
			setSpinner(true);
			sendRequest(null, rootApiUrl + "stories/" + taskId, "GET")
				.then((response) => {
					setSpinner(false);
					setTask(response)
				})
				.catch((error)=>{
					console.log(error);
                    try {
						setMessage( JSON.parse(error.message).message);
					} catch (_){
						console.log(error);
						setMessage(String(error.status));
					}
                    setVisibility(true);
                });
		}, []
	);

    return (
		<div className={classes.wrapper}>
			<header className={classes.header}>
				<div><span>Task: </span> {task?.name}</div>
				<div>
					<FontAwesomeIcon icon={faEdit} className={classes.button}  size="lg"
						onClick={_ => navigate("/tasks/" + task.id + "/update")}/>
					<FontAwesomeIcon icon={faTrashAlt} className={classes.button}  size="lg"
						onClick={ (event) => {
							event.preventDefault();
							sendRequest(null, rootApiUrl + "stories/" + task.id, "DELETE")
								.then(() => navigate("/projects/"+task.projectId))
								.catch((error)=>{
                        			try {
										error = JSON.parse(error.message);
										setMessage(error.message);
									} catch (_){
										setMessage(String(error.statusCode));
									}
									setVisibility(true);
								});
						}}/>
				</div>
			</header>
			{task ? (
				<Task task={task} setMessage={setMessage} 
					setSpinner={setSpinner} setVisibility={setVisibility}/>
			) : null}
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</div>
	);


};


export default TaskPage;