import React, {useContext, useState, useEffect} from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import { Status, Priority, getKeyByValue } from './TaskEnums';
import classes from "../../scss/shared.module.scss"


function TaskUpdatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    let { taskId } = useParams();
    const [task, setTask] = useState();
    const [newTask, setNewTask] = useState();
    const [users, setUsers] = useState([]);
    const [user, setUser] = useState();
	const rootApiUrl =  useContext(UrlContext);

    function updateHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(newTask, rootApiUrl + "stories/", "PUT")
                    .then(() => navigate("/tasks/"+taskId))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }

    useEffect(
		() =>{
            setSpinner(true);
            sendRequest(null, rootApiUrl + "stories/" + taskId, "GET")
				.then((task) => {
                    setTask(task);
                    setNewTask(task);
		            if (task.userId!=null){
                        sendRequest(null, rootApiUrl + "users/" + task.userId, "GET")
                            .then((user) => setUser(user))
                            .catch((error)=>{
                                error = JSON.parse(error.message);
                                setMessage(error.message);
                                setVisibility(true);
                        });
                    }
                    sendRequest(null, rootApiUrl + "projects/" + task.projectId + "/members", "GET")
                        .then((users) => {
                            setSpinner(false);
                            setUsers(users)
                        })
                        .catch((error)=>{
                            error = JSON.parse(error.message);
                            setMessage(error.message);
                            setVisibility(true);
                    });
                })
				.catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
		},[]
	);


    return (
		<form onSubmit={updateHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>Update task</span>
                <FontAwesomeIcon icon={faSave} onClick={updateHandler} className={classes.button} size="lg"/>
            </header>
            <div className={classes.record}>
                <span>Name: </span>
                <input placeholder="Name..." value={newTask?.name} required
                    onChange={event => setNewTask({...newTask, name: event.target.value})}/>
            </div>
            <div className={classes.record}>
                <span>Description: </span>
                <input placeholder="Description..." value={newTask?.description} 
                    onChange={event => setNewTask({...newTask, description: event.target.value})}/>
            </div>
            <div className={classes.record}>
                <span>Assign to: </span>
                <select required onChange={event => setNewTask({...newTask, userId: event.target.value})}>
                    <option value={user? user.id: -1} selected>{user? user.fullName: "Unassigned"}</option>
                    {users?.map((otherUser) =>{
                        if (user?.id != otherUser.id){
                            return <option value={otherUser.id} key={otherUser.id}>{otherUser.fullName}</option>
                        }
                    })}
                </select>
            </div>
            <div className={classes.record}>
                <span>Status: </span>
                <select onChange={(event) => setNewTask({...newTask, status: parseInt(event.target.value)})}>
                    <option value={task?.status} selected>{getKeyByValue(Status, task?.status)}</option>
                    {Object.entries(Status).map(([name, value]) => {
                        if (value != task?.status){
                            return <option value={value} key={value}>{name}</option>
                        }
                    })}
                </select>
            </div>
            <div className={classes.record}>
                <span>Priority: </span>
                <select onChange={event => setNewTask({...newTask, priority: parseInt(event.target.value)})} >
                    <option value={task?.priority} selected>{getKeyByValue(Priority, task?.priority)}</option>
                    {Object.entries(Priority).map(([name, value]) => {
                        if (value != task?.priority){
                            return <option value={value} key={value}>{name}</option>
                        }
                    })}
                </select>
            </div>
            <div className={classes.record}>
                <span>Story points: </span>
                <input placeholder="" value={newTask?.storyPoints} 
                    onChange={event => setNewTask({...newTask, storyPoints: event.target.value})}/>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</form>
	);
};


export default TaskUpdatePage;