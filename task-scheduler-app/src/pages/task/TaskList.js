import { Link } from "react-router-dom";
import { Status, getKeyByValue, Priority } from "./TaskEnums"
import classes from '../../scss/list.module.scss';


function sortTasks(stories){
    var sortedStories = { ToDo : [], InProgress : [], Done: []}
    stories?.map(story => {
        const status = Object.keys(Status).find(key => Status[key] === story.status);
        sortedStories[status] = [story, ...sortedStories[status]];
    });    
    return sortedStories;
}

function TaskList({tasks}){
    return (
        <div className={classes.list} style={{"flexDirection":"row"}}>
            {Object.entries(sortTasks(tasks)).map(([status, stories]) =>{
                return (
                    <div key={status} className={classes.listWrapper}>
                        <span className={classes.record}>{status}</span>
                        <hr/>
                        <div className={classes.list}>
                            {stories?.map(story => {
                                return (
                                    <Link to={"/tasks/" + story.id} key={story.id} className={classes.listItem}>
                                        <div>{story.name}</div>
                                        <div className={classes.itemDetails}>
                                            <div>Priority: {getKeyByValue(Priority, story.priority)}</div>
                                            <div>Story points: {story.storyPoints}</div>
                                        </div>
                                    </Link>
                                );
                            })}
                        </div>
                    </div>
                )
            })}
        </div>
    );
};

export default TaskList;