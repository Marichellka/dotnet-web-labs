import React, {useContext, useState, useEffect} from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { Priority } from "./TaskEnums"
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss"

const emptyTask = {
    name: "",
	description: "",
	userId: null,
    priority: Priority.Low,
    storyPoints: 0,
}

function TaskCreatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    let { projectId } = useParams();
    const [newTask, setNewTask] = useState({...emptyTask, projectId: projectId});
    const [users, setUsers] = useState([null]);
    const [teams, setTeams] = useState([]);
	const rootApiUrl =  useContext(UrlContext);

    function createHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(newTask, rootApiUrl + "stories/", "POST")
                    .then(() => navigate("/projects/"+projectId))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }

    useEffect(
		() =>{
            setSpinner(true);
            sendRequest(null, rootApiUrl + "projects/" + projectId + "/members", "GET")
				.then((response) => setUsers([...users, ...response]))
				.catch((error)=>{
                    setMessage(parseError(error));
                    setVisibility(true);
                });

            sendRequest(null, rootApiUrl + "projects/" + projectId + "/teams", "GET")
				.then((response) => {
					setSpinner(false);
                    setTeams(response)
                })
				.catch((error)=>{
                    setMessage(parseError(error));
                    setVisibility(true);
                });
		}, []
	);

    return (
		<form onSubmit={createHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>Create task</span>
                <FontAwesomeIcon icon={faSave} onClick={createHandler} className={classes.button} size="lg"/>
            </header>
            <div className={classes.record}>
                <span>Name: </span>
                <input placeholder="Name..." value={newTask?.name} required
                    onChange={event => setNewTask({...newTask, name: event.target.value})}/>
            </div>
            <div className={classes.record}>
                <span>Description: </span>
                <input placeholder="Description..." value={newTask?.description} 
                    onChange={event => setNewTask({...newTask, description: event.target.value})}/>
            </div>
            <div className={classes.record}>
                <span>Assign to: </span>
                <select required onChange={event => setNewTask({...newTask, userId: event.target.value})}>
                    {users?.map((member) =>{
                        return <option value={member? member.id : null}  key={member? member.id: null}>{member? member.fullName: "Unassigned"}</option>
                    })}
                </select>
            </div>
            <div className={classes.record}>
                <span>Priority: </span>
                <select onChange={event => setNewTask({...newTask, priority: parseInt(event.target.value)})} >
                    {Object.entries(Priority).map(([name, value]) => {
                        return <option value={value} key={value}>{name}</option>
                    })}
                </select>
            </div>
            <div className={classes.record}>
                <span>Story points: </span>
                <input placeholder="" value={newTask.storyPoints} 
                    onChange={event => setNewTask({...newTask, storyPoints: event.target.value})}/>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</form>
	);


};


export default TaskCreatePage;