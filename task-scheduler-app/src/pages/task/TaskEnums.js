const Status = 
{
    ToDo : 0,
    InProgress : 1,
    Done : 2
};
    
const Priority = 
{
    Low: 0,
    Medium: 1,
    High: 2
};

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}

export {Priority, Status, getKeyByValue};