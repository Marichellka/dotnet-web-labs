import React, {useContext, useState, useEffect} from 'react';
import { useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss";

function UserUpdatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    let { userId } = useParams();
    const [team, setTeam] = useState();
    const [teams, setTeams] = useState([]);
    const [user, setUser] = useState();
	const rootApiUrl =  useContext(UrlContext);

    function updateHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(user, rootApiUrl + "users/", "PUT")
                    .then(navigate("/users/"+userId))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }

    useEffect(
		() =>{
            setSpinner(true);
            sendRequest(null, rootApiUrl + "users/" + userId, "GET")
				.then((user) => {
                    delete user.email;
                    setUser(user);
                    sendRequest(null, rootApiUrl + "teams/" + user.teamId, "GET")
                    .then((project) => {
                        setTeam(project);
                        sendRequest(null, rootApiUrl + "projects/" + project.projectId + "/teams", "GET")
                            .then((teams) => {
                                setSpinner(true);
                                setTeams(teams)
                            })
                        .catch((error)=>{
                            setMessage(parseError(error));
                            setVisibility(true);
                        });                
                    })
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
                })
				.catch((error)=>{
                    setMessage(parseError(error));
                    setVisibility(true);
                });
		},[]
	);


    return (
		<form onSubmit={updateHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>Update user</span>
                <FontAwesomeIcon icon={faSave} onClick={updateHandler} className={classes.button} size="lg"/>
            </header>
            <div className={classes.record}>
                <span>Full Name: </span>
                <input placeholder="Name..." value={user?.fullName} required
                    onChange={event => setUser({...user, fullName: event.target.value})}/>
            </div>
            <div className={classes.record}>
                <span>Team: </span>
                <select required onChange={event => setUser({...user, teamId: event.target.value})}>
                    <option value={team?.id} selected>{team?.name}</option>
                    {teams?.map((otherTeam) =>{
                        if (team.Id != otherTeam.id){
                            return <option value={otherTeam} key={otherTeam.id}>{otherTeam.name}</option>
                        }
                    })}
                </select>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</form>
	);


};


export default UserUpdatePage;