import React, {useContext, useState, useEffect} from 'react';
import { Link, useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import classes from "../../scss/shared.module.scss";

const emptyUser = {
    fullName: "",
	email: "",
}

function UserCreatePage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
    let { teamId } = useParams();
    const [newUser, setNewUser] = useState({...emptyUser, teamId: teamId});
	const rootApiUrl =  useContext(UrlContext);

    function createHandler(event) {
        setSpinner(true);
        event.preventDefault();
        sendRequest(newUser, rootApiUrl + "users/", "POST")
                    .then(() => navigate("/teams/"+teamId))
                    .catch((error)=>{
                        setMessage(parseError(error));
                        setVisibility(true);
                    });
    }


    return (
		<form onSubmit={createHandler} className={classes.wrapper}>
            <header className={classes.header}>
                <span>Update user</span>
                <FontAwesomeIcon icon={faSave} onClick={createHandler} className={classes.button} size="lg"/>
            </header>
            <div className={classes.record}>
                <span>Full Name: </span>
                <input placeholder="Name..." value={newUser.fullName} required
                    onChange={event => setNewUser({...newUser, fullName: event.target.value})}/>
            </div>
            <div className={classes.record}>
                <span>Email: </span>
                <input type="email" placeholder="Email..." value={newUser.email} required
                    onChange={event => setNewUser({...newUser, email: event.target.value})}/>
            </div>
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</form>
	);
};


export default UserCreatePage;