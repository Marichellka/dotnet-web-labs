import { Link } from "react-router-dom";
import classes from '../../scss/list.module.scss';

function UserList({users}){
    return (
        <div className={classes.list}>
            {users.map(user => {
                return (
                    <Link to={"/users/" + user.id} key={user.id} className={classes.listItem}>
                        {user.fullName}
                    </Link>
                );
            })}
        </div>
    );
};

export default UserList;