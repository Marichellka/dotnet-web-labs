import React, {useContext, useState, useEffect} from 'react';
import { Link, useParams, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit } from "@fortawesome/free-solid-svg-icons";
import { UrlContext } from "../../App";
import { parseError, sendRequest } from "../../common/utils";
import Message from "../../common/Message";
import Spinner from "../../common/Spinner";
import TaskList from '../task/TaskList';
import classes from "../../scss/shared.module.scss"

function User ({user, setMessage, setSpinner, setVisibility}) {
    const [team, setTeam] = useState();
	const [tasks, setTasks] = useState([]);
	const rootApiUrl =  useContext(UrlContext);

    useEffect(() => {
        setSpinner(true);
		sendRequest(null, rootApiUrl + "teams/" + user.teamId, "GET")
			.then((response) => setTeam(response))
			.catch((error)=>{
				setMessage(parseError(error));
				setVisibility(true);
            });

		sendRequest(null, rootApiUrl + "users/" + user.id + "/stories", "GET")
			.then((response) => {
				setSpinner(false);
				setTasks(response)
			})
			.catch((error)=>{
				setMessage(parseError(error));
				setVisibility(true);
            });
	}, []);

    return (
        <div>
			<hr/>
			<div className={classes.record}><span>Email: </span>{user.email}</div>
			<div className={classes.record}>
				<span>Team: </span>
				<Link to={"/teams/" + user.teamId} className={classes.link}>
                	{team? team.name: ""}
            	</Link>
			</div>
			<hr/>
            <TaskList tasks={tasks}/>
		</div>
    );
};

function UserPage () {
    const navigate = useNavigate();
	const [messageVisibility, setVisibility] = useState(false);
	const [spinnerVisibility, setSpinner] = useState(false);
	const [message, setMessage] = useState("");
	let { userId } = useParams();
    const [user, setUser] = useState();
	const rootApiUrl =  useContext(UrlContext);

    useEffect(() =>{
        setSpinner(true);
		sendRequest(null, rootApiUrl + "users/" + userId, "GET")
				.then((response) => {
					setSpinner(false);
					setUser(response)
				})
				.catch((error)=>{
					setMessage(parseError(error));
					setVisibility(true);
                });
	},[]);

    return (
		<div className={classes.wrapper}>
			<header className={classes.header}>
				<div>
					<span>Employee: </span> 
					{user?.fullName}
				</div>
				<div>
					<FontAwesomeIcon icon={faEdit} className={classes.button}  size="lg"
						onClick={_ => navigate("/users/" + user.id + "/update")}/>
					<FontAwesomeIcon icon={faTrashAlt} className={classes.button} size="lg" 
						onClick={ (event) => {
							event.preventDefault();
							sendRequest(null, rootApiUrl + "users/" + user.id, "DELETE")
								.then(()=>navigate("/teams/"+user.teamId))
								.catch((error)=>{
									setMessage(parseError(error));
									setVisibility(true);
								});
						}}/>
				</div>
			</header>
			{user ? (
				<User user={user}  setMessage={setMessage} 
					setSpinner={setSpinner} setVisibility={setVisibility}/>
			) : null}
            <Message visibility={messageVisibility} message={message} setVisibility={(state) => {
                setVisibility(state);
                setSpinner(false);
            }} />
            <Spinner visibility={spinnerVisibility} />
		</div>
	);
};


export default UserPage;