import React, { createContext } from "react";
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import HomePage from './pages/home/HomePage';
import ProjectPage from './pages/project/ProjectPage';
import TaskPage from './pages/task/TaskPage';
import TeamPage from './pages/team/TeamPage';
import UserPage from './pages/user/UserPage';
import TaskCreatePage from './pages/task/TaskCreate';
import UserCreatePage from "./pages/user/UserCreate";
import ProjectsCreatePage from "./pages/project/ProjectCreate";
import TeamsCreatePage from "./pages/team/TeamCreate";
import UserUpdatePage from "./pages/user/UserUpdate";
import TeamUpdatePage from "./pages/team/TeamUpdate";
import TaskUpdatePage from "./pages/task/TaskUpdate";
import ProjectUpdatePage from "./pages/project/ProjectUpdate";

const apiUrl = "https://localhost:7267/api/";
const UrlContext = createContext(apiUrl);

function App() {
	return (
		<UrlContext.Provider value={apiUrl}>
			<Router>
				<Routes>
					<Route path="/" element={<HomePage />} />
					<Route path="/projects/" element={<HomePage />} />
					<Route path="/projects/:projectId" element={<ProjectPage />} />
					<Route path="/projects/:projectId/update" element={<ProjectUpdatePage />} />
					<Route path="/tasks/:taskId" element={<TaskPage />} />
					<Route path="/tasks/:taskId/update" element={<TaskUpdatePage />} />
					<Route path="/teams/:teamId" element={<TeamPage />} />
					<Route path="/teams/:teamId/update" element={<TeamUpdatePage />} />
					<Route path="/users/:userId" element={<UserPage />} />
					<Route path="/users/:userId/update" element={<UserUpdatePage />} />
					<Route path="/projects/:projectId/create-task" element={<TaskCreatePage />} />
					<Route path="/projects/create" element={<ProjectsCreatePage />} />
					<Route path="/teams/:teamId/create-user" element={<UserCreatePage />} />
					<Route path="/projects/:projectId/create-team" element={<TeamsCreatePage />} />
				</Routes>
			</Router>
		</UrlContext.Provider>
  );
}

export default App;
export { UrlContext };
