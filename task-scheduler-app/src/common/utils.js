async function sendRequest(credentials, url, httpMethod) {
	return fetch(url, {
		method: httpMethod,
		headers: {
			"Content-Type": "application/json",
			"Access-Control-Allow-Credentials": true,
			'Access-Control-Allow-Origin': '*',
			accept: "text/plain",
		},
		body: credentials ? JSON.stringify(credentials) : null,
		credentials: "include",
	}).then(async (response) => {
		const text = await response.text();
		if (response.ok) {
			return text === "" ? null : JSON.parse(text);
		}
		throw new Error(text);
	});
}

function parseError(error){
	error = JSON.parse(error.message);
	var message = error.message;
    if (message == undefined) {
		message = error.status + ": " + error.title +"\n";
		Object.values(error.errors).map(e => message += e[0] + "\n");
    }
	else{
		message += ":\n" + error.errors;
	}
	return message;
}

export {sendRequest, parseError};