import React from 'react';
import ClockLoader from "react-spinners/ClockLoader";
import classes from '../scss/spinner.module.scss';

function Spinner({visibility}) {
    return( 
        <div className={`${classes.spinner} ${visibility? ``: classes.none}`}>
            <ClockLoader loading={true} size={150}/>
        </div>
    );
};


export default Spinner;