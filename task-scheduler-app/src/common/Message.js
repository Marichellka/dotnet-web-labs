import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import classes from "../scss/message.module.scss";

function Message({visibility, setVisibility, message}){
    return (
        <div className={`${classes.message} ${visibility? ``: classes.none}`} >
            <span>Error</span>
            {message}
            <FontAwesomeIcon icon={faCheck}  className={classes.button} size="lg"
				onClick={_ => setVisibility(false)}/>
        </div>
    );
};

export default Message;