﻿using AutoMapper;
using TaskScheduler.BLL.Services;
using TaskScheduler.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.MappingProfiles;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Contexts;

namespace TaskScheduler.BLL.Tests.Tests;

public class UserTests
{
    private  UserService _userService;
    private  IUnitOfWork _unitOfWork;
    private  TestDbContext _context;
    
    [SetUp]
    public void SetUp()
    {
        var builder = new DbContextOptionsBuilder<AppDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();
        
        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<StoryProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        Mapper mapper = new Mapper(configuration);
        _userService = new UserService(_unitOfWork, mapper);
    }

    [Test]
    public async Task GetAll_EmptyDB_ReturnedEmptyList()
    {
        var returnedList = await _userService.GetAll();
        
        Assert.IsEmpty(returnedList);
    }

    [Test]
    public async Task GetAll_NotEmptyDB_ReturnedList()
    {
        await _userService.Create(new NewUserDto()
        {
            FullName = "Test",
            Email = "email@gmail.com"
        });
        await _userService.Create(new NewUserDto()
        {
            FullName = "Test2",
            Email = "email2@gmail.com"
        });

        List<UserDto> expectedList = new List<UserDto>()
        {
            new UserDto() {Id = 1, FullName = "Test", Email = "email@gmail.com"},
            new UserDto() {Id = 2, FullName = "Test2", Email = "email2@gmail.com"},
        };

        var returnedList = await _userService.GetAll();

        Assert.That(returnedList, Is.EqualTo(expectedList));
    }
    
    [Test]
    public async Task GetAllUserStories_EmptyDB_ReturnedEmptyList()
    {
        var user = await _userService.Create(new NewUserDto()
        {
            FullName = "Test2",
            Email = "email2@gmail.com"
        });
        
        var returnedList = await _userService.GetAllUserStories(user.Id);
        
        Assert.IsEmpty(returnedList);
    }
    
    [Test]
    public async Task GetAllUserStories_NotEmptyDB_ReturnedEmptyList()
    {
        var user = await _userService.Create(new NewUserDto()
        {
            FullName = "Test2",
            Email = "email2@gmail.com"
        });
        
        await _unitOfWork.StoryRepository.Add(new Story
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low,
            UserId = user.Id
        });
        
        await _unitOfWork.StoryRepository.Add(new Story
        {
            Id = 2,
            Name = "Test2",
            Description = "some description",
            Priority = Story.StoryPriority.High,
            UserId = user.Id
        });

        await  _unitOfWork.SaveChangesAsync();
        
        List<StoryDto> expectedList = new List<StoryDto>()
        {
            new StoryDto()
            {
                Id = 1, Name = "Test", Description = "some description", Priority = Story.StoryPriority.Low,
                UserId = user.Id
            },
            new StoryDto()
            {
                Id = 2, Name = "Test2", Description = "some description", Priority = Story.StoryPriority.High,
                UserId = user.Id
            },
        };
        
        var returnedList = await _userService.GetAllUserStories(user.Id);
        
        Assert.That(expectedList, Is.EqualTo(returnedList));
    }
    
    [Test]
    public async Task GetById_GetExistingUser_ReturnedUser()
    {
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            FullName = "Test",
            Email = "email@gmail.com"
        });
        await _unitOfWork.SaveChangesAsync();
        var expectedUser = new UserDto()
        {
            Id = 1,
            FullName = "Test",
            Email = "email@gmail.com"
        };

        var returnedUser = await _userService.GetById(1);
        
        Assert.That(returnedUser, Is.EqualTo(expectedUser));
    }
    
    [Test]
    public void GetById_GetNonExistingUser_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _userService.GetById(1));
    }
    
    [Test]
    public async Task CreateUser_WhenNewUserPassed_UserCreated()
    {
        var newUser = new NewUserDto()
        {
            FullName = "Test",
            Email = "email@gmail.com"
        };
        var expected = new UserDto()
        {
            Id = 1,
            FullName = "Test",
            Email = "email@gmail.com"
        };

        var createdUser = await _userService.Create(newUser);
        
        Assert.That(createdUser, Is.EqualTo(expected));
    }

    [Test]
    public async Task UpdateUser_UpdateExistingUser_UserUpdated()
    {
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            FullName = "Test",
            Email = "email@gmail.com"
        });
        await _unitOfWork.SaveChangesAsync();

        UpdateUserDto updatedUser = new UpdateUserDto()
        {
            Id = 1,
            FullName = "Test2",
        };
        
        UserDto expected = new UserDto()
        {
            Id = 1,
            FullName = "Test2",
            Email = "email@gmail.com"
        };

        var actualUser = await _userService.Update(updatedUser);
        
        Assert.That(actualUser, Is.EqualTo(expected));
    }
    
    [Test]
    public void UpdateUser_UpdateNonExistingUser_ThrownNotFoundException()
    {
        UpdateUserDto updatedUser = new UpdateUserDto()
        {
            Id = 1,
            FullName = "Test2",
        };

        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _userService.Update(updatedUser));
    }
    
    [Test]
    public async Task DeleteUser_DeleteExistingUser_UserDeleted()
    {
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            FullName = "Test",
            Email = "email@gmail.com"
        });
        await _unitOfWork.SaveChangesAsync();

        await _userService.Delete(1);
        
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _userService.GetById(1));
    }
    
    [Test]
    public void DeleteUser_DeleteNonExistingUser_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _userService.Delete(1));
    }

    [Test]
    public async Task AddToTeam_AddToExistingTeam_UserAdded()
    {
        var team = new Team()
        {
            Id = 1,
            Name = "Test"
        };
        await _unitOfWork.TeamRepository.Add(team);
        await _unitOfWork.SaveChangesAsync();

        var user = new NewUserDto()
        {
            FullName = "Test",
            Email = "email@gmail.com",
            TeamId = 1
        };

        await _userService.Create(user);

        Assert.That(team.Employees, Is.EqualTo(new List<User>()
        {
            new User()
            {
                Id = 1, FullName = "Test",
                Email = "email@gmail.com",
                TeamId = 1
            }
        }));
    }
    
    [Test]
    public void AddToTeam_AddToNotExistingTeam_ThrownNotFoundException()
    {
        var user = new NewUserDto()
        {
            FullName = "Test",
            Email = "email@gmail.com",
            TeamId = 2
        };

        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _userService.Create(user));
    }
}