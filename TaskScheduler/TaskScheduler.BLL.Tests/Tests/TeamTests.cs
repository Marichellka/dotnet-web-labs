﻿using AutoMapper;
using TaskScheduler.BLL.Services;
using TaskScheduler.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.MappingProfiles;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Contexts;

namespace TaskScheduler.BLL.Tests.Tests;

public class TeamTests
{
    private  TeamService _teamService;
    private  IUnitOfWork _unitOfWork;
    private  TestDbContext _context;
    
    [SetUp]
    public void SetUp()
    {
        var builder = new DbContextOptionsBuilder<AppDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();
        
        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<StoryProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        Mapper mapper = new Mapper(configuration);
        _teamService = new TeamService(_unitOfWork, mapper);
    }

    [Test]
    public async Task GetAll_EmptyDB_ReturnedEmptyList()
    {
        var returnedList = await _teamService.GetAll();
        
        Assert.IsEmpty(returnedList);
    }

    [Test]
    public async Task GetAll_NotEmptyDB_ReturnedList()
    {
        await _teamService.Create(new NewTeamDto()
        {
            Name = "Test"
        });
        await _teamService.Create(new NewTeamDto()
        {
            Name = "Test2"
        });

        List<TeamDto> expectedList = new List<TeamDto>()
        {
            new TeamDto() {Id = 1, Name = "Test"},
            new TeamDto() {Id = 2, Name = "Test2"},
        };

        var returnedList = await _teamService.GetAll();

        Assert.That(returnedList, Is.EqualTo(expectedList));
    }
    
    [Test]
    public async Task GetAllTeamMembers_NotEmptyDB_ReturnedList()
    {
        var team = await _teamService.Create(new NewTeamDto()
        {
            Name = "Test2",
        });
        
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 1,
            FullName = "Test",
            Email = "email@gmail.com",
            TeamId = team.Id
        });
        
        await _unitOfWork.UserRepository.Add(new User
        {
            Id = 2,
            FullName = "Test2",
            Email = "email2@gmail.com",
            TeamId = team.Id
        });

        await  _unitOfWork.SaveChangesAsync();
        
        List<UserDto> expectedList = new List<UserDto>()
        {
            new UserDto() {Id = 1, FullName = "Test", Email = "email@gmail.com", TeamId = team.Id},
            new UserDto() {Id = 2, FullName = "Test2", Email = "email2@gmail.com", TeamId = team.Id},
        };
        
        var returnedList = await _teamService.GetAllTeamMembers(team.Id);
        
        Assert.That(expectedList, Is.EqualTo(returnedList));
    }
    
    [Test]
    public async Task GetAllTeamMembers_EmptyDB_ReturnedEmptyList()
    {
        var team = await _teamService.Create(new NewTeamDto()
        {
            Name = "Test2",
        });
        
        var returnedList = await _teamService.GetAllTeamMembers(team.Id);
        
        Assert.IsEmpty(returnedList);
    }
    
    [Test]
    public async Task GetById_GetExistingTeam_ReturnedTeam()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            Name = "Test",
        });
        await _unitOfWork.SaveChangesAsync();
        var expectedTeam = new TeamDto()
        {
            Id = 1,
            Name = "Test",
        };

        var returnedTeam = await _teamService.GetById(1);
        
        Assert.That(returnedTeam, Is.EqualTo(expectedTeam));
    }
    
    [Test]
    public void GetById_GetNonExistingTeam_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _teamService.GetById(1));
    }
    
    [Test]
    public async Task CreateTeam_WhenNewTeamPassed_TeamCreated()
    {
        var newTeam = new NewTeamDto()
        {
            Name = "Test",
        };
        
        var expected = new TeamDto()
        {
            Id = 1,
            Name = "Test",
        };

        var createdTeam = await _teamService.Create(newTeam);
        
        Assert.That(createdTeam, Is.EqualTo(expected));
    }

    [Test]
    public async Task UpdateTeam_UpdateExistingTeam_TeamUpdated()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            Name = "Test",
        });
        await _unitOfWork.SaveChangesAsync();

        UpdateTeamDto updatedTeam = new UpdateTeamDto()
        {
            Id = 1,
            Name = "Test2",
        };
        TeamDto expected = new TeamDto()
        {
            Id = 1,
            Name = "Test2",
        };

        var actualTeam = await _teamService.Update(updatedTeam);
        
        Assert.That(actualTeam, Is.EqualTo(expected));
    }
    
    [Test]
    public void UpdateTeam_UpdateNonExistingTeam_ThrownNotFoundException()
    {
        UpdateTeamDto updatedTeam = new UpdateTeamDto()
        {
            Id = 1,
            Name = "Test2",
        };

        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _teamService.Update(updatedTeam));
    }
    
    [Test]
    public async Task DeleteTeam_DeleteExistingTeam_TeamDeleted()
    {
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            Name = "Test",
        });
        await _unitOfWork.SaveChangesAsync();

        await _teamService.Delete(1);
        
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _teamService.GetById(1));
    }
    
    [Test]
    public void DeleteTeam_DeleteNonExistingTeam_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _teamService.Delete(1));
    }
    
    [Test]
    public async Task AddToProject_AddToExistingProject_TeamAdded()
    {
        var project = new Project()
        {
            Id = 1,
            Name = "Test",
            Description = "some description"
        };
        await _unitOfWork.ProjectRepository.Add(project);
        await _unitOfWork.SaveChangesAsync();

        var team = new NewTeamDto()
        {
            Name = "Test",
            ProjectId = 1
        };
        List<Team> expected = new List<Team>() { new Team() { Id = 1, Name = "Test", ProjectId = 1}};

        await _teamService.Create(team);

        Assert.That(project.Teams, Is.EqualTo(expected));
    }
    
    [Test]
    public void AddToProject_AddToNotExistingProject_ThrownNotFoundException()
    {
        var team = new NewTeamDto()
        {
            Name = "Test",
            ProjectId = 1
        };
        
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _teamService.Create(team));
    }
}