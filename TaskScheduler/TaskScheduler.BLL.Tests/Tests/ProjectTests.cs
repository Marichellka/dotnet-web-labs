﻿using AutoMapper;
using TaskScheduler.BLL.Services;
using TaskScheduler.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.MappingProfiles;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Contexts;

namespace TaskScheduler.BLL.Tests.Tests;

public class ProjectTests
{
    private  ProjectService _projectService;
    private  IUnitOfWork _unitOfWork;
    private  TestDbContext _context;
    
    [SetUp]
    public void SetUp()
    {
        var builder = new DbContextOptionsBuilder<AppDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();
        
        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<StoryProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        Mapper mapper = new Mapper(configuration);
        _projectService = new ProjectService(_unitOfWork, mapper);
    }
    
    [Test]
    public async Task GetAllProjectStories_EmptyDB_ReturnedEmptyList()
    {
        var project = await _projectService.Create(new NewProjectDto()
        {
            Name = "Test2",
        });
        
        var returnedList = await _projectService.GetAllProjectStories(project.Id);
        
        Assert.IsEmpty(returnedList);
    }
    
    [Test]
    public async Task GetAllProjectTeams_NotEmptyDB_ReturnedList()
    {
        var project = await _projectService.Create(new NewProjectDto()
        {
            Name = "Test2",
        });
        
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 1,
            Name = "Test",
            ProjectId = project.Id
        });
        
        await _unitOfWork.TeamRepository.Add(new Team
        {
            Id = 2,
            Name = "Test2",
            ProjectId = project.Id
        });

        await  _unitOfWork.SaveChangesAsync();
        
        List<TeamDto> expectedList = new List<TeamDto>()
        {
            new TeamDto() {Id = 1, Name = "Test", ProjectId = project.Id},
            new TeamDto() {Id = 2, Name = "Test2", ProjectId = project.Id},
        };
        
        var returnedList = await _projectService.GetAllProjectTeams(project.Id);
        
        Assert.That(expectedList, Is.EqualTo(returnedList));
    }
    
    [Test]
    public async Task GetAllProjectTeams_EmptyDB_ReturnedEmptyList()
    {
        var project = await _projectService.Create(new NewProjectDto()
        {
            Name = "Test2",
        });
        
        var returnedList = await _projectService.GetAllProjectTeams(project.Id);
        
        Assert.IsEmpty(returnedList);
    }
    
    [Test]
    public async Task GetAllProjectStories_NotEmptyDB_ReturnedList()
    {
        var project = await _projectService.Create(new NewProjectDto()
        {
            Name = "Test2",
        });
        
        await _unitOfWork.StoryRepository.Add(new Story
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low,
            ProjectId = project.Id
        });
        
        await _unitOfWork.StoryRepository.Add(new Story
        {
            Id = 2,
            Name = "Test2",
            Description = "some description",
            Priority = Story.StoryPriority.High,
            ProjectId = project.Id
        });

        await  _unitOfWork.SaveChangesAsync();
        
        List<StoryDto> expectedList = new List<StoryDto>()
        {
            new StoryDto()
            {
                Id = 1, Name = "Test", Description = "some description", Priority = Story.StoryPriority.Low,
                ProjectId = project.Id
            },
            new StoryDto()
            {
                Id = 2, Name = "Test2", Description = "some description", Priority = Story.StoryPriority.High,
                ProjectId = project.Id
            },
        };
        
        var returnedList = await _projectService.GetAllProjectStories(project.Id);
        
        Assert.That(expectedList, Is.EqualTo(returnedList));
    }

    [Test]
    public async Task GetAll_EmptyDB_ReturnedEmptyList()
    {
        var returnedList = await _projectService.GetAll();
        
        Assert.IsEmpty(returnedList);
    }

    [Test]
    public async Task GetAll_NotEmptyDB_ReturnedList()
    {
        await _projectService.Create(new NewProjectDto()
        {
            Name = "Test",
            Description = "some description"
        });
        await _projectService.Create(new NewProjectDto()
        {
            Name = "Test2",
            Description = "email2@gmail.com"
        });

        List<ProjectDto> expectedList = new List<ProjectDto>()
        {
            new ProjectDto() {Id = 1, Name = "Test", Description = "some description"},
            new ProjectDto() {Id = 2, Name = "Test2", Description = "email2@gmail.com"},
        };

        var returnedList = await _projectService.GetAll();

        Assert.That(returnedList, Is.EqualTo(expectedList));
    }
    
    [Test]
    public async Task GetById_GetExistingProject_ReturnedProject()
    {
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            Name = "Test",
            Description = "some description"
        });
        await _unitOfWork.SaveChangesAsync();
        var expectedProject = new ProjectDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description"
        };

        var returnedProject = await _projectService.GetById(1);
        
        Assert.That(returnedProject, Is.EqualTo(expectedProject));
    }
    
    [Test]
    public void GetById_GetNonExistingProject_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _projectService.GetById(1));
    }
    
    [Test]
    public async Task CreateProject_WhenNewProjectPassed_ProjectCreated()
    {
        var newProject = new NewProjectDto()
        {
            Name = "Test",
            Description = "some description"
        };
        var expected = new ProjectDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description"
        };

        var createdProject = await _projectService.Create(newProject);
        
        Assert.That(createdProject, Is.EqualTo(expected));
    }

    [Test]
    public async Task UpdateProject_UpdateExistingProject_ProjectUpdated()
    {
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            Name = "Test",
            Description = "some description"
        });
        await _unitOfWork.SaveChangesAsync();

        UpdateProjectDto updatedProject = new UpdateProjectDto
        {
            Id = 1,
            Name = "Test2",
            Description = "some description2"
        };
        
        ProjectDto expectedProject = new ProjectDto
        {
            Id = 1,
            Name = "Test2",
            Description = "some description2"
        };

        var actualProject = await _projectService.Update(updatedProject);
        
        Assert.That(actualProject, Is.EqualTo(expectedProject));
    }
    
    [Test]
    public void UpdateProject_UpdateNonExistingProject_ThrownNotFoundException()
    {
        UpdateProjectDto updatedProject = new UpdateProjectDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description"
        };

        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _projectService.Update(updatedProject));
    }
    
    [Test]
    public async Task DeleteProject_DeleteExistingProject_ProjectDeleted()
    {
        await _unitOfWork.ProjectRepository.Add(new Project
        {
            Id = 1,
            Name = "Test",
            Description = "some description"
        });
        await _unitOfWork.SaveChangesAsync();

        await _projectService.Delete(1);
        
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _projectService.GetById(1));
    }
    
    [Test]
    public void DeleteProject_DeleteNonExistingProject_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _projectService.Delete(1));
    }
    
}