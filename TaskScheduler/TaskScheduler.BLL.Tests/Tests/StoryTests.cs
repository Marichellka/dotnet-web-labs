﻿using AutoMapper;
using TaskScheduler.BLL.Services;
using TaskScheduler.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.MappingProfiles;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Contexts;

namespace TaskScheduler.BLL.Tests.Tests;

public class StoryTests
{
    private  StoryService _storyService;
    private  IUnitOfWork _unitOfWork;
    private  TestDbContext _context;
    
    [SetUp]
    public void SetUp()
    {
        var builder = new DbContextOptionsBuilder<AppDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();
        
        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<ProjectProfile>();
            cfg.AddProfile<StoryProfile>();
            cfg.AddProfile<TeamProfile>();
            cfg.AddProfile<UserProfile>();
        });
        Mapper mapper = new Mapper(configuration);
        _storyService = new StoryService(_unitOfWork, mapper);
    }

    [Test]
    public async Task GetAll_EmptyDB_ReturnedEmptyList()
    {
        var returnedList = await _storyService.GetAll();
        
        Assert.IsEmpty(returnedList);
    }

    [Test]
    public async Task GetAll_NotEmptyDB_ReturnedList()
    {
        await _storyService.Create(new NewStoryDto()
        {
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low
        });
        await _storyService.Create(new NewStoryDto()
        {
            Name = "Test2",
            Description = "some description",
            Priority = Story.StoryPriority.High
        });

        List<StoryDto> expectedList = new List<StoryDto>()
        {
            new StoryDto()
            {
                Id = 1, Name = "Test", Description = "some description", Priority = Story.StoryPriority.Low,
                Status = Story.StoryStatus.ToDo
            },
            new StoryDto()
            {
                Id = 2, Name = "Test2", Description = "some description", Priority = Story.StoryPriority.High,
                Status = Story.StoryStatus.ToDo
            },
        };

        var returnedList = await _storyService.GetAll();

        Assert.That(returnedList, Is.EqualTo(expectedList));
    }
    
    [Test]
    public async Task GetById_GetExistingStory_ReturnedStory()
    {
        await _unitOfWork.StoryRepository.Add(new Story
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low
        });
        await _unitOfWork.SaveChangesAsync();
        var expectedStory = new StoryDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low
        };

        var returnedStory = await _storyService.GetById(1);
        
        Assert.That(returnedStory, Is.EqualTo(expectedStory));
    }
    
    [Test]
    public void GetById_GetNonExistingStory_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _storyService.GetById(1));
    }
    
    [Test]
    public async Task CreateStory_WhenNewStoryPassed_StoryCreated()
    {
        var newStory = new NewStoryDto()
        {
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low
        };
        
        var expectedStory = new StoryDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low,
            Status = Story.StoryStatus.ToDo
        };

        var createdStory = await _storyService.Create(newStory);
        
        Assert.That(createdStory, Is.EqualTo(expectedStory));
    }

    [Test]
    public async Task UpdateStory_UpdateExistingStory_StoryUpdated()
    {
        await _unitOfWork.StoryRepository.Add(new Story
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low
        });
        await _unitOfWork.SaveChangesAsync();

        UpdateStoryDto updatedStory = new UpdateStoryDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low,
            Status = Story.StoryStatus.Done
            
        };
        
        StoryDto expectedStory = new StoryDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low,
            Status = Story.StoryStatus.Done
            
        };

        var actualStory = await _storyService.Update(updatedStory);
        
        Assert.That(actualStory, Is.EqualTo(expectedStory));
    }
    
    [Test]
    public void UpdateStory_UpdateNonExistingStory_ThrownNotFoundException()
    {
        UpdateStoryDto updatedStory = new UpdateStoryDto()
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low
        };

        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _storyService.Update(updatedStory));
    }
    
    [Test]
    public async Task DeleteStory_DeleteExistingStory_StoryDeleted()
    {
        await _unitOfWork.StoryRepository.Add(new Story
        {
            Id = 1,
            Name = "Test",
            Description = "some description",
            Priority = Story.StoryPriority.Low
        });
        await _unitOfWork.SaveChangesAsync();

        await _storyService.Delete(1);
        
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _storyService.GetById(1));
    }
    
    [Test]
    public void DeleteStory_DeleteNonExistingStory_ThrownNotFoundException()
    {
        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _storyService.Delete(1));
    }
    
    [Test]
    public async Task AddToProject_AddToExistingProject_StoryAdded()
    {
        var project = new Project()
        {
            Id = 1,
            Name = "test"
        };
        await _unitOfWork.ProjectRepository.Add(project);
        await _unitOfWork.SaveChangesAsync();

        var story = new NewStoryDto()
        {
            Name = "Test",
            ProjectId = 1
        };

        List<Story> expected = new List<Story>()
        {
            new Story()
            {
                Id = 1,
                Name = "Test",
                ProjectId = 1,
                Status = Story.StoryStatus.ToDo
            }
        };

        await _storyService.Create(story);

        Assert.That(project.Stories, Is.EqualTo(expected));
    }
    
    [Test]
    public void AddToProject_AddToNotExistingProject_ThrownNotFoundException()
    {
        var story = new NewStoryDto()
        {
            Name = "Test",
            ProjectId = 1
        };

        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _storyService.Create(story));
    }
    
    [Test]
    public async Task AssignToUser_AssignToExistingUser_StoryAssigned()
    {
        var user = new User()
        {
            Id = 1,
            FullName = "name",
            Email = "email@gmail.com"
        };
        await _unitOfWork.UserRepository.Add(user);
        await _unitOfWork.SaveChangesAsync();

        var story = new NewStoryDto()
        {
            Name = "Test",
            UserId = 1
        };
        
        List<Story> expected = new List<Story>()
        {
            new Story()
            {
                Id = 1,
                Name = "Test",
                UserId = 1,
                Status = Story.StoryStatus.ToDo
            }
        };

        await _storyService.Create(story);
        
        Assert.That(user.Stories, Is.EqualTo(expected));
    }
    
    [Test]
    public void AssignToUser_AssignToNotExistingUser_ThrownNotFoundException()
    {
        var story = new NewStoryDto()
        {
            Name = "Test",
            UserId = 1
        };

        Assert.ThrowsAsync(typeof(NotFoundException), async Task () => await _storyService.Create(story));
    }
}