﻿using Microsoft.EntityFrameworkCore;
using TaskScheduler.DAL.Contexts;

namespace TaskScheduler.BLL.Tests;

public class TestDbContext : AppDbContext
{
    public TestDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
    }
}