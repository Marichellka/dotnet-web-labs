﻿using System.Reflection;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.BLL.MappingProfiles;
using TaskScheduler.BLL.Services;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Contexts;
using TaskScheduler.WebAPI.Validators.Project;
using TaskScheduler.WebAPI.Validators.Story;
using TaskScheduler.WebAPI.Validators.Team;
using TaskScheduler.WebAPI.Validators.User;

namespace TaskScheduler.WebAPI.Extensions;

public static class ServiceExtensions
{
    public static void RegisterServices(this IServiceCollection services)
    {
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IProjectService, ProjectService>();
        services.AddScoped<IStoryService, StoryService>();
        services.AddScoped<ITeamService, TeamService>();

        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<AppDbContext>();
    }
    
    public static  void ConfigureServices(this IServiceCollection services)
    {
        services.AddAutoMapper(cfg => {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<StoryProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>(); 
            },
            Assembly.GetExecutingAssembly());
    }
    
    public static void RegisterValidators(this IServiceCollection services)
    {
        services.AddSingleton<IValidator<UserDto>, UserDtoValidator>();
        services.AddSingleton<IValidator<NewUserDto>, NewUserDtoValidator>();
        services.AddSingleton<IValidator<UpdateUserDto>, UpdateUserDtoValidator>();

        services.AddSingleton<IValidator<ProjectDto>, ProjectDtoValidator>();
        services.AddSingleton<IValidator<NewProjectDto>, NewProjectDtoValidator>();
        services.AddSingleton<IValidator<UpdateProjectDto>, UpdateProjectDtoValidator>();

        services.AddSingleton<IValidator<StoryDto>, StoryDtoValidator>();
        services.AddSingleton<IValidator<NewStoryDto>, NewStoryDtoValidator>();
        services.AddSingleton<IValidator<UpdateStoryDto>, UpdateStoryDtoValidator>();

        services.AddSingleton<IValidator<TeamDto>, TeamDtoValidator>();
        services.AddSingleton<IValidator<NewTeamDto>, NewTeamDtoValidator>();
        services.AddSingleton<IValidator<UpdateTeamDto>, UpdateTeamDtoValidator>();
    }
    
    public static void ConfigureValidationErrors(this IServiceCollection services)
    {
        services.Configure<ApiBehaviorOptions>(options =>
        {
            options.InvalidModelStateResponseFactory = context =>
            {
                var errors = context.ModelState.Values.SelectMany(x => x.Errors.Select(p => p.ErrorMessage)).ToList();
                var result = new
                {
                    Message = "Validation errors",
                    Errors = errors
                };

                return new BadRequestObjectResult(result);
            };
        });
    }
}