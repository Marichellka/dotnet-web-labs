﻿using FluentValidation;
using TaskScheduler.BLL.DTO.Story;

namespace TaskScheduler.WebAPI.Validators.Story;

public class NewStoryDtoValidator: AbstractValidator<NewStoryDto>
{
    public NewStoryDtoValidator(IEnumerable<StoryDto> teams)
    {
        RuleFor(s => s.Name)
            .NotEmpty()
            .MaximumLength(100)
            .WithMessage("Name should be maximum 100 characters.");

        RuleFor(s => s.Name)
            .SetValidator(new UniqueValidator<StoryDto>(teams))
            .WithMessage("Name is already taken.");

        RuleFor(s => s.StoryPoints)
            .GreaterThanOrEqualTo(0)
            .WithMessage("Story points must be non negative.");
    }
}