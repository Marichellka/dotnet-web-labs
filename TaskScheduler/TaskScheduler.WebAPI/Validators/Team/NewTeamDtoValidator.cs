﻿using FluentValidation;
using TaskScheduler.BLL.DTO.Team;

namespace TaskScheduler.WebAPI.Validators.Team;

public class NewTeamDtoValidator : AbstractValidator<NewTeamDto>
{
    public NewTeamDtoValidator(IEnumerable<TeamDto> teams)
    {
        RuleFor(u => u.Name)
            .NotEmpty()
            .MaximumLength(100)
            .WithMessage("Name should be maximum 100 characters.");

        RuleFor(u => u.Name)
            .SetValidator(new UniqueValidator<TeamDto>(teams))
            .WithMessage("Name is already taken.");
    }
}