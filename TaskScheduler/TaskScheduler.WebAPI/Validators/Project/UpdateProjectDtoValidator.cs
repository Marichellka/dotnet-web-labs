﻿using FluentValidation;
using TaskScheduler.BLL.DTO.Project;

namespace TaskScheduler.WebAPI.Validators.Project;

public class UpdateProjectDtoValidator : AbstractValidator<UpdateProjectDto>
{
    public UpdateProjectDtoValidator(IEnumerable<ProjectDto> teams)
    {
        RuleFor(u => u.Name)
            .NotEmpty()
            .MaximumLength(100)
            .WithMessage("Name should be maximum 100 characters.");

        RuleFor(u => u.Name)
            .SetValidator(new UniqueValidator<ProjectDto>(teams))
            .WithMessage("Name is already taken.");
    }
}