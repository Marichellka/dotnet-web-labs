﻿using FluentValidation;
using TaskScheduler.BLL.DTO.Project;

namespace TaskScheduler.WebAPI.Validators.Project;

public class ProjectDtoValidator : AbstractValidator<ProjectDto>
{
    public ProjectDtoValidator(IEnumerable<ProjectDto> teams)
    {
        RuleFor(u => u.Name)
            .NotEmpty()
            .MaximumLength(100)
            .WithMessage("Name should be maximum 100 characters.");

        RuleFor(u => u.Name)
            .SetValidator(new UniqueValidator<ProjectDto>(teams))
            .WithMessage("Name is already taken.");
    }
}