﻿using FluentValidation;
using TaskScheduler.BLL.DTO.Project;

namespace TaskScheduler.WebAPI.Validators.Project;

public class NewProjectDtoValidator : AbstractValidator<NewProjectDto>
{
    public NewProjectDtoValidator(IEnumerable<ProjectDto> projects)
    {
        RuleFor(u => u.Name)
            .NotEmpty()
            .MaximumLength(100)
            .WithMessage("Name should be maximum 100 characters.");

        RuleFor(u => u.Name)
            .SetValidator(new UniqueValidator<ProjectDto>(projects))
            .WithMessage("Name is already taken.");
    }
}