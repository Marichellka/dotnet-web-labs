﻿using System.Reflection;
using FluentValidation;
using FluentValidation.Validators;

namespace TaskScheduler.WebAPI.Validators;

public class UniqueValidator<T> : PropertyValidator
    where T: class
{
    private readonly IEnumerable<T> _items;

    public UniqueValidator(IEnumerable<T> items)
        : base("{PropertyName} must be unique")
    {
        _items = items;
    }

    protected override bool IsValid(PropertyValidatorContext context)
    {
        var editedItem = context.InstanceToValidate as T;
        var newValue = context.PropertyValue as string;
        var property = typeof(T).GetTypeInfo().GetDeclaredProperty(context.PropertyName);
        return _items.All(item => 
            item.Equals(editedItem) || property.GetValue(item).ToString() != newValue);
    }
    
    public static IRuleBuilderOptions<TItem, TProperty> IsUnique<TItem, TProperty>(
        IRuleBuilder<TItem, TProperty> ruleBuilder, IEnumerable<TItem> items)
        where TItem : class
    {
        return ruleBuilder.SetValidator(new UniqueValidator<TItem>(items));
    }
}