﻿using FluentValidation;
using TaskScheduler.BLL.DTO.User;

namespace TaskScheduler.WebAPI.Validators.User;

public class UpdateUserDtoValidator: AbstractValidator<UpdateUserDto>
{
    public UpdateUserDtoValidator(IEnumerable<UserDto> users)
    {
        RuleFor(u => u.FullName)
            .NotEmpty()
            .MaximumLength(100)
            .WithMessage("Name should be maximum 100 characters.");
        
        RuleFor(u => u.FullName)
            .SetValidator(new UniqueValidator<UserDto>(users))
            .WithMessage("Full name is already taken.");
    }
}