﻿using FluentValidation;
using TaskScheduler.BLL.DTO.User;

namespace TaskScheduler.WebAPI.Validators.User;

public class NewUserDtoValidator : AbstractValidator<NewUserDto>
{
    public NewUserDtoValidator(IEnumerable<NewUserDto> users)
    {
        RuleFor(u => u.Email)
            .NotEmpty()
            .MaximumLength(100)
            .EmailAddress();
        
        RuleFor(u => u.Email)
            .SetValidator(new UniqueValidator<NewUserDto>(users))
            .WithMessage("Email is already taken.");

        RuleFor(u => u.FullName)
            .NotEmpty()
            .MaximumLength(100)
            .WithMessage("Name should be maximum 100 characters.");
        
        RuleFor(u => u.FullName)
            .SetValidator(new UniqueValidator<NewUserDto>(users))
            .WithMessage("Full name is already taken.");
    }
}