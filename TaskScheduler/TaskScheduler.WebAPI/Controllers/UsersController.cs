﻿using AutoMapper;
using FluentValidation.AspNetCore;
using FluentValidation.Validators;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.BLL.Services;
using TaskScheduler.WebAPI.Validators;
using TaskScheduler.WebAPI.Validators.User;

namespace TaskScheduler.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UsersController: ControllerBase
{
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public UsersController(IUserService userService, IMapper mapper)
    {
        _userService = userService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<ICollection<UserDto>>> Get()
    {
        return Ok(await _userService.GetAll());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<UserDto>> GetById(int id)
    {
        return Ok(await _userService.GetById(id));
    }
    
    [HttpGet("{userId}/stories")]
    public async Task<ActionResult<IEnumerable<StoryDto>>> GetAllProjectStories(int userId)
    {
        return Ok(await _userService.GetAllUserStories(userId));
    }

    [HttpPost]
    public async Task<ActionResult<UserDto>> Create([FromBody] NewUserDto user)
    {
        var validator = new UserDtoValidator(await _userService.GetAll());
        UserDto createdUser = _mapper.Map<UserDto>(user);
        var validationResult = validator.Validate(createdUser);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        
        return Ok(await _userService.Create(user));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody] UpdateUserDto user)
    {
        var validator = new UserDtoValidator(await _userService.GetAll());
        UserDto updatedUser = _mapper.Map<UserDto>(user);
        var validationResult = validator.Validate(updatedUser);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        
        return Ok(await _userService.Update(user));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _userService.Delete(id);
        return NoContent();
    }
    
    public override ActionResult ValidationProblem([ActionResultObjectValue] ModelStateDictionary modelStateDictionary)
    {
        var options = HttpContext.RequestServices.GetRequiredService<IOptions<ApiBehaviorOptions>>();
        return (ActionResult)options.Value.InvalidModelStateResponseFactory(ControllerContext);
    }
}