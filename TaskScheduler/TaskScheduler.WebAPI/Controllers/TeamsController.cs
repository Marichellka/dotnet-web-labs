﻿using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.BLL.Services;
using TaskScheduler.WebAPI.Validators.Team;

namespace TaskScheduler.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class TeamsController : ControllerBase
{
    private readonly ITeamService _teamService;
    private readonly IMapper _mapper;


    public TeamsController(ITeamService teamService, IMapper mapper)
    {
        _teamService = teamService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<ICollection<TeamDto>>> Get()
    {
        return Ok(await _teamService.GetAll());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TeamDto>> GetById(int id)
    {
        return Ok(await _teamService.GetById(id));
    }
    
    [HttpGet("{teamId}/members")]
    public async Task<ActionResult<IEnumerable<UserDto>>> GetAllProjectStories(int teamId)
    {
        return Ok(await _teamService.GetAllTeamMembers(teamId));
    }


    [HttpPost]
    public async Task<ActionResult<TeamDto>> Create([FromBody] NewTeamDto team)
    {
        var validator = new TeamDtoValidator(await _teamService.GetAll());
        TeamDto newTeam = _mapper.Map<TeamDto>(team);
        var validationResult = validator.Validate(newTeam);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        return Ok(await _teamService.Create(team));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody] UpdateTeamDto team)
    {
        var validator = new TeamDtoValidator(await _teamService.GetAll());
        TeamDto updatedTeam = _mapper.Map<TeamDto>(team);
        var validationResult = validator.Validate(updatedTeam);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        return Ok(await _teamService.Update(team));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _teamService.Delete(id);
        return NoContent();
    }
    
    public override ActionResult ValidationProblem([ActionResultObjectValue] ModelStateDictionary modelStateDictionary)
    {
        var options = HttpContext.RequestServices.GetRequiredService<IOptions<ApiBehaviorOptions>>();
        return (ActionResult)options.Value.InvalidModelStateResponseFactory(ControllerContext);
    }
}