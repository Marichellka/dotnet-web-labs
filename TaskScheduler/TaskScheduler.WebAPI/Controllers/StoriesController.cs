﻿using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.DAL.Models;
using TaskScheduler.WebAPI.Validators.Story;

namespace TaskScheduler.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class StoriesController : ControllerBase
{
    private readonly IStoryService _storyService;
    private readonly IMapper _mapper;


    public StoriesController(IStoryService storyService, IMapper mapper)
    {
        _storyService = storyService;
        _mapper = mapper;
    }
    
    [HttpGet]
    public async Task<ActionResult<ICollection<StoryDto>>> Get()
    {
        return Ok(await _storyService.GetAll());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<StoryDto>> GetById(int id)
    {
        return Ok(await _storyService.GetById(id));
    }

    [HttpPost]
    public async Task<ActionResult<StoryDto>> Create([FromBody] NewStoryDto story)
    {
        var validator = new StoryDtoValidator(await _storyService.GetAll());
        StoryDto newStory = _mapper.Map<StoryDto>(story);
        var validationResult = validator.Validate(newStory);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        return Ok(await _storyService.Create(story));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody] UpdateStoryDto story)
    {
        var validator = new StoryDtoValidator(await _storyService.GetAll());
        StoryDto updatedStory = _mapper.Map<StoryDto>(story);
        var validationResult = validator.Validate(updatedStory);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        return Ok(await _storyService.Update(story));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _storyService.Delete(id);
        return NoContent();
    }
    
    public override ActionResult ValidationProblem([ActionResultObjectValue] ModelStateDictionary modelStateDictionary)
    {
        var options = HttpContext.RequestServices.GetRequiredService<IOptions<ApiBehaviorOptions>>();
        return (ActionResult)options.Value.InvalidModelStateResponseFactory(ControllerContext);
    }
}