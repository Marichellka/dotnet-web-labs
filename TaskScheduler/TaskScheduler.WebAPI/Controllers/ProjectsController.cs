﻿using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.DAL.Models;
using TaskScheduler.WebAPI.Validators.Project;

namespace TaskScheduler.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ProjectsController: ControllerBase
{
    private readonly IProjectService _projectService;
    private readonly IMapper _mapper;

    public ProjectsController(IProjectService projectService, IMapper mapper)
    {
        _projectService = projectService;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<ActionResult<ICollection<ProjectDto>>> Get()
    {
        return Ok(await _projectService.GetAll());
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<ProjectDto>> GetById(int id)
    {
        return Ok(await _projectService.GetById(id));
    }
    
    [HttpGet("{projectId}/teams")]
    public async Task<ActionResult<IEnumerable<TeamDto>>> GetAllProjectTeams(int projectId)
    {
        return Ok(await _projectService.GetAllProjectTeams(projectId));
    }
    
    [HttpGet("{projectId}/stories")]
    public async Task<ActionResult<IEnumerable<StoryDto>>> GetAllProjectStories(int projectId)
    {
        return Ok(await _projectService.GetAllProjectStories(projectId));
    }
    
    [HttpGet("{projectId}/members")]
    public async Task<ActionResult<IEnumerable<UserDto>>> GetAllProjectMembers(int projectId)
    {
        return Ok(await _projectService.GetAllProjectMembers(projectId));
    }


    [HttpPost]
    public async Task<ActionResult<ProjectDto>> Create([FromBody] NewProjectDto project)
    {
        var validator = new ProjectDtoValidator(await _projectService.GetAll());
        ProjectDto newProject = _mapper.Map<ProjectDto>(project);
        var validationResult = validator.Validate(newProject);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        return Ok(await _projectService.Create(project));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody] UpdateProjectDto project)
    {
        var validator = new ProjectDtoValidator(await _projectService.GetAll());
        ProjectDto createdProject = _mapper.Map<ProjectDto>(project);
        var validationResult = validator.Validate(createdProject);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState, null);
            return ValidationProblem(ModelState);  
        }
        return Ok(await _projectService.Update(project));
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
        await _projectService.Delete(id);
        return NoContent();
    }
    
    public override ActionResult ValidationProblem([ActionResultObjectValue] ModelStateDictionary modelStateDictionary)
    {
        var options = HttpContext.RequestServices.GetRequiredService<IOptions<ApiBehaviorOptions>>();
        return (ActionResult)options.Value.InvalidModelStateResponseFactory(ControllerContext);
    }
}