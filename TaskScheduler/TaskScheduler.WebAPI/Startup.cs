﻿using System.Reflection;
using FluentValidation.AspNetCore;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using TaskScheduler.BLL.MappingProfiles;
using TaskScheduler.DAL.Contexts;
using TaskScheduler.DAL.Repositories;
using TaskScheduler.WebAPI.Extensions;

namespace TaskScheduler.WebAPI;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddDbContext<AppDbContext>(options =>
            options.UseSqlServer(Configuration["ConnectionStrings:SchedulerDBConnection"]));

        services.AddCors();
        
        services.AddControllers();
        services.AddSwaggerGen(c =>
        {
            c.CustomSchemaIds(type => type.ToString());
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "TaskScheduler.WebAPI", Version = "v1" });
        });

        services.RegisterServices();
        services.ConfigureServices();
        services.RegisterValidators();
        services.ConfigureValidationErrors();
        
        services
            .AddMvcCore()
            .AddFluentValidation();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TaskScheduler.WebAPI v1"));
        }

        app.UseExceptionHandler("/Error");

        app.UseRouting();

        app.UseCors(opt => opt
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .SetIsOriginAllowed(_ => true));
        
        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}