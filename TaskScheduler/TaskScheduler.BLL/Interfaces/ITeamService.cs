﻿using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;

namespace TaskScheduler.BLL.Interfaces;

public interface ITeamService
{
    Task<ICollection<TeamDto>> GetAll();

    Task<ICollection<UserDto>> GetAllTeamMembers(int teamId);

    Task<TeamDto> GetById(int id);

    Task<TeamDto> Create(NewTeamDto team);

    Task<TeamDto> Update(UpdateTeamDto updatedTeam);

    Task Delete(int id);
}