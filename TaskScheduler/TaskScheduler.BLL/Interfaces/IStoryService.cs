﻿using TaskScheduler.BLL.DTO.Story;

namespace TaskScheduler.BLL.Interfaces;

public interface IStoryService
{
    Task<ICollection<StoryDto>> GetAll();

    Task<StoryDto> GetById(int id);

    Task<StoryDto> Create(NewStoryDto story);

    Task<StoryDto> Update(UpdateStoryDto updatedStory);

    Task Delete(int id);
}