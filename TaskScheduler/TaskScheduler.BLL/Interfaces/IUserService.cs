﻿using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.User;

namespace TaskScheduler.BLL.Interfaces;

public interface IUserService
{
    Task<ICollection<UserDto>> GetAll();

    Task<ICollection<StoryDto>> GetAllUserStories(int userId);

    Task<UserDto> GetById(int id);

    Task<UserDto> Create(NewUserDto user);

    Task<UserDto> Update(UpdateUserDto updatedUser);

    Task Delete(int id);
}