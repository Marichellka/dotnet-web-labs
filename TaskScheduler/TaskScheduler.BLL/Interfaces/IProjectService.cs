﻿using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;

namespace TaskScheduler.BLL.Interfaces;

public interface IProjectService
{
    Task<ICollection<ProjectDto>> GetAll();

    Task<ICollection<TeamDto>> GetAllProjectTeams(int projectId);

    Task<ICollection<StoryDto>> GetAllProjectStories(int projectId);

    Task<ICollection<UserDto>> GetAllProjectMembers(int projectId);

    Task<ProjectDto> GetById(int id);
    
    Task<ProjectDto> Create(NewProjectDto newProject);

    Task<ProjectDto> Update(UpdateProjectDto updatedProject);

    Task Delete(int id);
}