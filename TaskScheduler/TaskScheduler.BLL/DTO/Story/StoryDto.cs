﻿using static TaskScheduler.DAL.Models.Story;

namespace TaskScheduler.BLL.DTO.Story;

public class StoryDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    public int? UserId { get; set; }
    public StoryStatus? Status { get; set; }
    public StoryPriority? Priority { get; set; }
    public int? ProjectId { get; set; }
    public int StoryPoints { get; set; }
    
    public override bool Equals(object? obj)
    {
        if (obj is not StoryDto)
            return false;
        return Equals((StoryDto)obj);
    }

    protected bool Equals(StoryDto other)
    {
        return Id == other.Id &&Name == other.Name && Description == other.Description && UserId == other.UserId &&
               Status == other.Status && Priority == other.Priority && ProjectId == other.ProjectId &&
               StoryPoints == other.StoryPoints;
    }
}