﻿using static TaskScheduler.DAL.Models.Story;

namespace TaskScheduler.BLL.DTO.Story;

public class NewStoryDto
{
    public string? Name { get; set; }
    
    public string? Description { get; set; }
    
    public int? UserId { get; set; }
    
    public StoryPriority? Priority { get; set; }
    
    public int? ProjectId { get; set; }
    
    public int StoryPoints { get; set; }
}