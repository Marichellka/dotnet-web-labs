﻿using static TaskScheduler.DAL.Models.Story;

namespace TaskScheduler.BLL.DTO.Story;

public class UpdateStoryDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    public int? UserId { get; set; }
    public StoryStatus? Status { get; set; }
    public StoryPriority? Priority { get; set; }
    public int StoryPoints { get; set; }
}