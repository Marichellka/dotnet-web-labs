﻿namespace TaskScheduler.BLL.DTO.User;

public class NewUserDto
{
    public string? FullName { get; set; }
    public string? Email { get; set; }
    public int? TeamId { get; set; }
}