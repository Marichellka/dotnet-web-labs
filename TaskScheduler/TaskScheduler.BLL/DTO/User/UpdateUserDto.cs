﻿namespace TaskScheduler.BLL.DTO.User;

public class UpdateUserDto
{
    public int Id { get; set; }
    public string? FullName { get; set; }
    public int? TeamId { get; set; }
}