﻿namespace TaskScheduler.BLL.DTO.User;

public class UserDto
{    
    public int Id { get; set; }
    public string? FullName { get; set; }
    public string? Email { get; set; }
    public int? TeamId { get; set; }
    
    public override bool Equals(object? obj)
    {
        if (obj is not UserDto)
            return false;
        return Equals((UserDto)obj);
    }

    protected bool Equals(UserDto other)
    {
        return  Id == other.Id && FullName == other.FullName && Email == other.Email && TeamId == other.TeamId;
    }
}