﻿namespace TaskScheduler.BLL.DTO.Project;

public class ProjectDto
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    
    public override bool Equals(object? obj)
    {
        if (obj is not ProjectDto)
            return false;
        return Equals((ProjectDto)obj);
    }
    
    protected bool Equals(ProjectDto other)
    {
        return Id == other.Id && Name == other.Name && Description == other.Description;
    }
}