﻿namespace TaskScheduler.BLL.DTO.Project;

public class NewProjectDto
{
    public string? Name { get; set; }
    public string? Description { get; set; }
}