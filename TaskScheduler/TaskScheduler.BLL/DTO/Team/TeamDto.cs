﻿namespace TaskScheduler.BLL.DTO.Team;

public class TeamDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int? ProjectId { get; set; }
    
    public override bool Equals(object? obj)
    {
        if (obj is not TeamDto)
            return false;
        return Equals((TeamDto)obj);
    }

    protected bool Equals(TeamDto other)
    {
        return Name == other.Name && ProjectId == other.ProjectId;
    }
}