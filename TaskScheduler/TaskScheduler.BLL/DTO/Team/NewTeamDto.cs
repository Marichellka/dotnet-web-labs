﻿namespace TaskScheduler.BLL.DTO.Team;

public class NewTeamDto
{
    public string? Name { get; set; }
    public int? ProjectId { get; set; }
}