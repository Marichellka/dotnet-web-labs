﻿using AutoMapper;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.Services;

public class TeamService:  BaseService, ITeamService
{
    public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }

    public async Task<ICollection<TeamDto>> GetAll()
    {
        var teams = await UnitOfWork.TeamRepository.GetAll();
        return Mapper.Map<ICollection<TeamDto>>(teams);
    }
    
    
    public async Task<ICollection<UserDto>> GetAllTeamMembers(int teamId)
    {
        var users = await UnitOfWork.UserRepository.GetAll();
        return Mapper.Map<ICollection<UserDto>>(users.Where(u => u.TeamId == teamId));
    }

    public async Task<TeamDto> GetById(int id)
    {
        var team = await UnitOfWork.TeamRepository.GetById(id) ??
                      throw new NotFoundException(nameof(Team), id);

        return Mapper.Map<TeamDto>(team);
    }
    
    public async Task<TeamDto> Create(NewTeamDto team)
    {
        if (team.ProjectId is not null)
        {
            var _ = await UnitOfWork.ProjectRepository.GetById(team.ProjectId.Value) ?? 
                    throw new NotFoundException(nameof(Project), team.ProjectId.Value);
        }

        var teamEntity = Mapper.Map<Team>(team);
        await UnitOfWork.TeamRepository.Add(teamEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<TeamDto>(teamEntity);
    }

    public async Task<TeamDto> Update(UpdateTeamDto updatedTeam)
    {
        var team = await UnitOfWork.TeamRepository.GetById(updatedTeam.Id) ??
                      throw new NotFoundException(nameof(Team), updatedTeam.Id);

        team.Name = updatedTeam.Name;

        await UnitOfWork.TeamRepository.Update(team);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<TeamDto>(team);
    }

    public async Task Delete(int id)
    {
        var team = await UnitOfWork.TeamRepository.GetById(id) ??
                            throw new NotFoundException(nameof(Team), id);

        await UnitOfWork.TeamRepository.Delete(team);
        await UnitOfWork.SaveChangesAsync();
    }
}