﻿using AutoMapper;
using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.Services;

public class ProjectService: BaseService, IProjectService
{
    public ProjectService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }
    
    public async Task<ICollection<ProjectDto>> GetAll()
    {
        var projects = await UnitOfWork.ProjectRepository.GetAll();
        return Mapper.Map<ICollection<ProjectDto>>(projects);
    }
    
    public async Task<ICollection<TeamDto>> GetAllProjectTeams(int projectId)
    {
        var teams = await UnitOfWork.TeamRepository.GetAll();
        return Mapper.Map<ICollection<TeamDto>>(teams.Where(t => t.ProjectId == projectId));
    }
    
    public async Task<ICollection<StoryDto>> GetAllProjectStories(int projectId)
    {
        var stories = await UnitOfWork.StoryRepository.GetAll();
        return Mapper.Map<ICollection<StoryDto>>(stories.Where(s => s.ProjectId == projectId));
    }

    public async Task<ICollection<UserDto>> GetAllProjectMembers(int projectId)
    {
        var teams = await GetAllProjectTeams(projectId);
        var teamIds = teams.Select(t => t.Id).ToList();

        var users = await UnitOfWork.UserRepository.GetAll();
        return Mapper.Map<ICollection<UserDto>>(users.Where(u => u.TeamId is { } id && teamIds.Contains(id)));
    }

    public async Task<ProjectDto> GetById(int id)
    {
        var project = await UnitOfWork.ProjectRepository.GetById(id) ??
                            throw new NotFoundException(nameof(Project), id);

        return Mapper.Map<ProjectDto>(project);
    }
    
    public async Task<ProjectDto> Create(NewProjectDto newProject)
    {
        var project = Mapper.Map<Project>(newProject);
        await UnitOfWork.ProjectRepository.Add(project);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<ProjectDto>(project);
    }

    public async Task<ProjectDto> Update(UpdateProjectDto updatedProject)
    {
        var project = await UnitOfWork.ProjectRepository.GetById(updatedProject.Id) ??
                      throw new NotFoundException(nameof(Project), updatedProject.Id);

        project.Name = updatedProject.Name;
        project.Description = updatedProject.Description;

        await UnitOfWork.ProjectRepository.Update(project);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<ProjectDto>(project);
    }

    public async Task Delete(int id)
    {
        var project = await UnitOfWork.ProjectRepository.GetById(id) ??
                            throw new NotFoundException(nameof(Project), id);

        await UnitOfWork.ProjectRepository.Delete(project);
        await UnitOfWork.SaveChangesAsync();
    }
}