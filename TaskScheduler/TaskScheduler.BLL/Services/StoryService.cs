﻿using AutoMapper;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.Services;

public class StoryService: BaseService, IStoryService
{
    public StoryService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }

    public async Task<ICollection<StoryDto>> GetAll()
    {
        var stories = await UnitOfWork.StoryRepository.GetAll();
        return Mapper.Map<ICollection<StoryDto>>(stories);
    }

    public async Task<StoryDto> GetById(int id)
    {
        var story = await UnitOfWork.StoryRepository.GetById(id) ??
                    throw new NotFoundException(nameof(Story), id);

        return Mapper.Map<StoryDto>(story);
    }

    public async Task<StoryDto> Create(NewStoryDto story)
    {
        if (story.ProjectId is not null)
        {
            var _ = await UnitOfWork.ProjectRepository.GetById(story.ProjectId.Value) ?? 
                       throw new NotFoundException(nameof(Project), story.ProjectId.Value);
        }
        
        if (story.UserId is not null)
        {
            var _ = await UnitOfWork.UserRepository.GetById(story.UserId.Value) ?? 
                        throw new NotFoundException(nameof(User), story.UserId.Value);
        }

        var storyEntity = Mapper.Map<Story>(story);
        storyEntity.Status = Story.StoryStatus.ToDo;
        await UnitOfWork.StoryRepository.Add(storyEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<StoryDto>(storyEntity);;
    }

    public async Task<StoryDto> Update(UpdateStoryDto updatedStory)
    {
        var story = await UnitOfWork.StoryRepository.GetById(updatedStory.Id) ??
                    throw new NotFoundException(nameof(Story), updatedStory.Id);

        story.Name = updatedStory.Name;
        story.Description = updatedStory.Description;
        story.Priority = updatedStory.Priority;
        story.Status = updatedStory.Status;
        story.StoryPoints = updatedStory.StoryPoints;
        
        if (updatedStory.UserId is not null)
        {
            var _ = await UnitOfWork.UserRepository.GetById(updatedStory.UserId.Value) ?? 
                    throw new NotFoundException(nameof(User), updatedStory.UserId.Value);
            story.UserId = updatedStory.UserId;
        }

        await UnitOfWork.StoryRepository.Update(story);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<StoryDto>(story);;
    }

    public async Task Delete(int id)
    {
        var story = await UnitOfWork.StoryRepository.GetById(id) ??
                    throw new NotFoundException(nameof(Story), id);

        await UnitOfWork.StoryRepository.Delete(story);
        await UnitOfWork.SaveChangesAsync();
    }
}