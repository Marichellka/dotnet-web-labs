﻿using AutoMapper;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.BLL.Exceptions;
using TaskScheduler.BLL.Interfaces;
using TaskScheduler.DAL;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.Services;

public class UserService: BaseService, IUserService
{
    public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
    {
    }
    
    public async Task<ICollection<UserDto>> GetAll()
    {
        var users = await UnitOfWork.UserRepository.GetAll();
        return Mapper.Map<ICollection<UserDto>>(users);
    }
    
    public async Task<ICollection<StoryDto>> GetAllUserStories(int userId)
    {
        var stories = await UnitOfWork.StoryRepository.GetAll();
        return Mapper.Map<ICollection<StoryDto>>(stories.Where(s => s.UserId == userId));
    }

    public async Task<UserDto> GetById(int id)
    {
        var user = await UnitOfWork.UserRepository.GetById(id) ??
                   throw new NotFoundException(nameof(User), id);

        return Mapper.Map<UserDto>(user);
    }
    
    public async Task<UserDto> Create(NewUserDto user)
    {
        if (user.TeamId is not null)
        {
            var _ = await UnitOfWork.TeamRepository.GetById(user.TeamId.Value) ?? 
                    throw new NotFoundException(nameof(Team), user.TeamId.Value);
        }
        
        
        var userEntity = Mapper.Map<User>(user);
        await UnitOfWork.UserRepository.Add(userEntity);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<UserDto>(userEntity);
    }

    public async Task<UserDto> Update(UpdateUserDto updatedUser)
    {
        var user = await UnitOfWork.UserRepository.GetById(updatedUser.Id) ??
                   throw new NotFoundException(nameof(User), updatedUser.Id);

        user.FullName = updatedUser.FullName;
        if (updatedUser.TeamId is not null)
        {
            var _ = await UnitOfWork.TeamRepository.GetById(updatedUser.TeamId.Value) ?? 
                    throw new NotFoundException(nameof(Team), updatedUser.TeamId.Value);
            user.TeamId = updatedUser.TeamId;
        }

        await UnitOfWork.UserRepository.Update(user);
        await UnitOfWork.SaveChangesAsync();

        return Mapper.Map<UserDto>(user);
    }

    public async Task Delete(int id)
    {
        var user = await UnitOfWork.UserRepository.GetById(id) ??
                   throw new NotFoundException(nameof(User), id);

        await UnitOfWork.UserRepository.Delete(user);
        await UnitOfWork.SaveChangesAsync();
    }
}