﻿using AutoMapper;
using TaskScheduler.BLL.DTO.Project;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.MappingProfiles;

public class ProjectProfile : Profile
{
    public ProjectProfile()
    {
        CreateMap<Project, ProjectDto>();
        CreateMap<NewProjectDto, Project>();
        CreateMap<Project, UpdateProjectDto>();
        CreateMap<UpdateProjectDto, ProjectDto>();
        CreateMap<NewProjectDto, ProjectDto>();
    }
}