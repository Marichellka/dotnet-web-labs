﻿using AutoMapper;
using TaskScheduler.BLL.DTO.User;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.MappingProfiles;

public class UserProfile : Profile
{
    public UserProfile()
    {
        CreateMap<User, UserDto>();
        CreateMap<NewUserDto, User>();
        CreateMap<UpdateUserDto, UserDto>();
        CreateMap<NewUserDto, UserDto>();
    }
}