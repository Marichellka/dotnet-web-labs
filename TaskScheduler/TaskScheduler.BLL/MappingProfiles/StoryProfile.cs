﻿using AutoMapper;
using TaskScheduler.BLL.DTO.Story;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.MappingProfiles;

public class StoryProfile: Profile
{
    public StoryProfile()
    {
        CreateMap<Story, StoryDto>();
        CreateMap<NewStoryDto, Story>();
        CreateMap<UpdateStoryDto, StoryDto>();
        CreateMap<NewStoryDto, StoryDto>();
    }
}