﻿using AutoMapper;
using TaskScheduler.BLL.DTO.Team;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.BLL.MappingProfiles;


public class TeamProfile : Profile
{
    public TeamProfile()
    {
        CreateMap<Team, TeamDto>();
        CreateMap<NewTeamDto, Team>();
        CreateMap<UpdateTeamDto, TeamDto>();
        CreateMap<NewTeamDto, TeamDto>();
    }
}