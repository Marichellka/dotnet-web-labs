﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TaskScheduler.DAL.Models;

public class Story:BaseModel
{
    [Required, MaxLength(200)]
    public string Name { get; set; }
    
    public string? Description { get; set; }
    
    public int? UserId { get; set; }
    
    public virtual User? User { get; set; }

    [DefaultValue(StoryStatus.ToDo)] 
    public StoryStatus? Status { get; set; }
    
    public StoryPriority? Priority { get; set; }
    
    public int? ProjectId { get; set; }
    public virtual Project? Project { get; set; }
    
    public int StoryPoints { get; set; }


    public enum StoryStatus
    {
        ToDo,
        InProgress,
        Done
    }
    
    public enum StoryPriority
    {
        Low,
        Medium,
        High
    }
    
    public override bool Equals(object? obj)
    {
        if (obj is not Story)
            return false;
        return Equals((Story)obj);
    }

    protected bool Equals(Story other)
    {
        return Name == other.Name && Description == other.Description && UserId == other.UserId &&
               Status == other.Status && Priority == other.Priority && ProjectId == other.ProjectId &&
               StoryPoints == other.StoryPoints;
    }
}