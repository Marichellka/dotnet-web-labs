﻿using System.ComponentModel.DataAnnotations;

namespace TaskScheduler.DAL.Models;

public class Team:BaseModel
{
    [Required, MaxLength(100)]
    public string Name { get; set; }
    
    public int? ProjectId { get; set; }
    
    public virtual Project? Project { get; set; }
    
    public virtual IEnumerable<User>? Employees { get; set; }
    
    public override bool Equals(object? obj)
    {
        if (obj is not Team)
            return false;
        return Equals((Team)obj);
    }

    protected bool Equals(Team other)
    {
        return Name == other.Name && ProjectId == other.ProjectId;
    }
}