﻿using System.ComponentModel.DataAnnotations;

namespace TaskScheduler.DAL.Models;

public class BaseModel
{
    [Key]
    public int Id { get; set; }
    
    protected bool Equals(BaseModel other)
    {
        return Id == other.Id;
    }
}