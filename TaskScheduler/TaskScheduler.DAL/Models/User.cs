﻿using System.ComponentModel.DataAnnotations;

namespace TaskScheduler.DAL.Models;

public class User : BaseModel
{
    [Required, MaxLength(100)]
    public string FullName { get; set; }
    
    [Required, EmailAddress]
    public string Email { get; set; }
    
    public int? TeamId { get; set; }
    public virtual Team? Team { get; set; }
    
    public virtual IEnumerable<Story>? Stories { get; set; }

    public override bool Equals(object? obj)
    {
        if (obj is not User)
            return false;
        return Equals((User)obj);
    }

    protected bool Equals(User other)
    {
        return base.Equals(other) && FullName == other.FullName && Email == other.Email && TeamId == other.TeamId;
    }
}