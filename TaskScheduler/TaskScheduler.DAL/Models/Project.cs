﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TaskScheduler.DAL.Models;

public class Project:BaseModel
{
    [Required, MaxLength(200)]
    public string Name { get; set; }

    public virtual IEnumerable<Story>? Stories { get; set; }
    
    public virtual IEnumerable<Team>? Teams { get; set; }

    public string? Description { get; set; }
    
    public override bool Equals(object? obj)
    {
        if (obj is not Project)
            return false;
        return Equals((Project)obj);
    }

    protected bool Equals(Project other)
    {
        return Name == other.Name && Description == other.Description;
    }
}