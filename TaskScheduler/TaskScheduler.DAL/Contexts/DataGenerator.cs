﻿using Bogus;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.DAL.Contexts;

public static class DataGenerator
{
    public static List<Team> GenerateRandomTeams(int teamsNumber, List<Project> projects)
    {
        var teamId = 1;

        var teamsFake = new Faker<Team>()
            .RuleFor(t => t.Id, f => teamId++)
            .RuleFor(t => t.ProjectId, f => f.PickRandom(projects).Id)
            .RuleFor(t => t.Name, f => f.Company.CompanyName());
        
        return teamsFake.Generate(teamsNumber);
    }

    public static List<User> GenerateRandomUsers(int usersNumber, List<Team> teams)
    {
        var userId = 1;

        var usersFake = new Faker<User>()
            .RuleFor(u => u.Id, f => userId++)
            .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id)
            .RuleFor(u => u.FullName, f => f.Name.FullName())
            .RuleFor(u => u.Email, f => f.Internet.Email());
        return usersFake.Generate(usersNumber);
    }

    public static List<Project> GenerateRandomProjects(int projectsNumber)
    {
        var projectId = 1;

        var projectsFake = new Faker<Project>()
            .RuleFor(p => p.Id, f => projectId++)
            .RuleFor(p => p.Name, f => f.Commerce.ProductName())
            .RuleFor(p => p.Description, f => f.Lorem.Sentence());
        
        return projectsFake.Generate(projectsNumber);
    }

    public static List<Story> GenerateRandomStories(int projectTasksNumber, List<User> users, List<Project> projects)
    {
        var storyId = 1;

        var storyFake = new Faker<Story>()
            .RuleFor(s => s.Id, f => storyId++)
            .RuleFor(s => s.ProjectId, f => f.PickRandom(projects).Id)
            .RuleFor(s => s.UserId, f => f.PickRandom(users).Id)
            .RuleFor(s => s.Name, f => f.Lorem.Sentence(2))
            .RuleFor(s => s.Description, f => f.Lorem.Sentence())
            .RuleFor(s => s.Status, f => f.PickRandom<Story.StoryStatus>())
            .RuleFor(s => s.Priority, f => f.PickRandom<Story.StoryPriority>())
            .RuleFor(s => s.StoryPoints, f => f.PickRandom(new [] {1, 2, 3, 4, 5, 6}));
        
        return storyFake.Generate(projectTasksNumber);
    }
}