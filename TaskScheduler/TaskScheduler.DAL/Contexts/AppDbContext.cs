﻿using Microsoft.EntityFrameworkCore;
using TaskScheduler.DAL.Models;

namespace TaskScheduler.DAL.Contexts
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> Employees { get; set; }

        public DbSet<Story> Tasks { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Team> Teams { get; set; }
        
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) 
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Story>()
                .HasOne(t => t.Project)
                .WithMany(s => s.Stories)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Cascade);
            
            builder.Entity<Story>()
                .HasOne(t => t.User)
                .WithMany(u => u.Stories)
                .HasForeignKey(t => t.UserId)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Team>()
                .HasOne(t => t.Project)
                .WithMany(p => p.Teams)
                .HasForeignKey(t => t.ProjectId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<User>()
                .HasOne(u => u.Team)
                .WithMany(t => t.Employees)
                .HasForeignKey(u => u.TeamId)
                .OnDelete(DeleteBehavior.SetNull);
            
            Seed(builder);

            base.OnModelCreating(builder);
        }
        
        public static void Seed(ModelBuilder modelBuilder)
        {
            var projects = DataGenerator.GenerateRandomProjects(5);
            var teams = DataGenerator.GenerateRandomTeams(12, projects);
            var users = DataGenerator.GenerateRandomUsers(50, teams);
            var stories = DataGenerator.GenerateRandomStories(200, users, projects);

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Story>().HasData(stories);
        }
    }
}