﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskScheduler.DAL.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    ProjectId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teams_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: true),
                    Priority = table.Column<int>(type: "int", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: true),
                    StoryPoints = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Employees_UserId",
                        column: x => x.UserId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Ad voluptate voluptate officiis totam natus quidem unde.", "Handmade Cotton Computer" },
                    { 2, "Consectetur dolores iusto vitae occaecati excepturi nulla enim occaecati adipisci.", "Rustic Fresh Mouse" },
                    { 3, "Ea qui quae impedit.", "Licensed Cotton Soap" },
                    { 4, "Consequatur sint pariatur accusamus et eveniet sapiente et rerum eligendi.", "Small Plastic Bike" },
                    { 5, "Rerum iste placeat fugit occaecati fuga labore tempora.", "Incredible Rubber Tuna" }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Name", "ProjectId" },
                values: new object[,]
                {
                    { 1, "Sanford, Keebler and Haag", 2 },
                    { 2, "Marquardt, Kreiger and Brown", 3 },
                    { 3, "Hoppe, Champlin and Mosciski", 1 },
                    { 4, "White Inc", 1 },
                    { 5, "Corkery, Mayert and Quigley", 5 },
                    { 6, "Lehner LLC", 1 },
                    { 7, "Labadie Inc", 2 },
                    { 8, "O'Kon, Bauch and Halvorson", 3 },
                    { 9, "Hoeger - Huel", 2 },
                    { 10, "Mueller - King", 3 },
                    { 11, "Kozey, Bartell and Bode", 4 },
                    { 12, "Erdman, Weissnat and Ritchie", 2 }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "Email", "FullName", "TeamId" },
                values: new object[,]
                {
                    { 1, "Oda.Glover98@hotmail.com", "Fae Medhurst", 6 },
                    { 2, "Wilhelmine_Prosacco70@hotmail.com", "Freddy Hickle", 4 },
                    { 3, "Gregory.Beatty@hotmail.com", "Maeve Purdy", 2 },
                    { 4, "Amalia_Lubowitz40@hotmail.com", "Alta Kunde", 6 },
                    { 5, "Aleen15@yahoo.com", "Milo Heidenreich", 5 },
                    { 6, "Albertha.Mayert@yahoo.com", "Ellsworth Dooley", 7 },
                    { 7, "Ross9@hotmail.com", "Delores Mante", 12 },
                    { 8, "Rachelle.Leuschke@gmail.com", "Maybell Halvorson", 8 },
                    { 9, "Giuseppe_Runolfsdottir@gmail.com", "Prince Cormier", 11 },
                    { 10, "Zackary_Hammes@yahoo.com", "Cody Jenkins", 10 },
                    { 11, "Chase_Kuvalis41@gmail.com", "Ariane Kozey", 4 },
                    { 12, "Shanna_Kuhic@yahoo.com", "Pasquale Jacobson", 2 },
                    { 13, "Maudie_Waelchi@gmail.com", "Alejandrin Kozey", 8 },
                    { 14, "Jamal44@yahoo.com", "Baby Towne", 8 },
                    { 15, "Vita.Lockman@hotmail.com", "Allison Botsford", 5 },
                    { 16, "Jerod.Ritchie8@yahoo.com", "Shannon Rodriguez", 12 },
                    { 17, "Regan0@yahoo.com", "Antonetta Walsh", 8 },
                    { 18, "Mariana_Hettinger90@yahoo.com", "Mossie Wolff", 2 },
                    { 19, "Arielle14@hotmail.com", "Lambert Williamson", 3 },
                    { 20, "Armani73@gmail.com", "Randall Bruen", 11 },
                    { 21, "Toby92@yahoo.com", "Marianne Reinger", 1 },
                    { 22, "Jensen.Kulas@hotmail.com", "Marcelle Abshire", 7 },
                    { 23, "Lyda92@hotmail.com", "Elwin Kassulke", 8 },
                    { 24, "Tito.Dare@yahoo.com", "Dustin Padberg", 5 },
                    { 25, "Janice_Gerhold@gmail.com", "Benedict Bayer", 9 },
                    { 26, "Anibal_Little@hotmail.com", "Kelsi Romaguera", 4 },
                    { 27, "Sheridan.McKenzie@hotmail.com", "Kenna Schultz", 10 },
                    { 28, "Mya33@yahoo.com", "Bud Anderson", 2 },
                    { 29, "Dayna89@yahoo.com", "Rogers Stehr", 2 },
                    { 30, "Zoey_Ullrich@gmail.com", "Lenny Johns", 8 },
                    { 31, "Taylor89@yahoo.com", "Herman Haley", 12 },
                    { 32, "April.Lakin@yahoo.com", "Emelia Heaney", 7 },
                    { 33, "Hannah_Bergstrom97@yahoo.com", "Ellie Nikolaus", 7 },
                    { 34, "Lea.Cummings7@gmail.com", "Burnice Rippin", 7 },
                    { 35, "Kari_Kovacek25@gmail.com", "Freida Lemke", 1 },
                    { 36, "Shaun.Hand88@gmail.com", "Jerald Feeney", 12 },
                    { 37, "Wayne_Shields8@gmail.com", "Polly Marks", 6 },
                    { 38, "Sherwood44@hotmail.com", "Adriel Sauer", 9 },
                    { 39, "Dean.Ryan@yahoo.com", "Oliver Bins", 5 },
                    { 40, "Chester.Robel@hotmail.com", "Adriel Rath", 4 },
                    { 41, "Herminio45@gmail.com", "Wayne Hansen", 12 },
                    { 42, "Willa11@yahoo.com", "Isaac Altenwerth", 1 }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "Email", "FullName", "TeamId" },
                values: new object[,]
                {
                    { 43, "Colton.Nolan@gmail.com", "Caroline Thiel", 12 },
                    { 44, "Felicia13@yahoo.com", "Eloisa Treutel", 9 },
                    { 45, "Cletus.Marvin54@gmail.com", "Fidel Dooley", 9 },
                    { 46, "Carolina.Block93@hotmail.com", "Agustin Denesik", 9 },
                    { 47, "Elizabeth_Greenfelder@hotmail.com", "Lindsay Haag", 10 },
                    { 48, "Felicita.King74@hotmail.com", "Elizabeth Kuhic", 2 },
                    { 49, "Gertrude_Kutch73@hotmail.com", "Erika Gleason", 3 },
                    { 50, "Adriel42@gmail.com", "Berta Howe", 9 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Description", "Name", "Priority", "ProjectId", "Status", "StoryPoints", "UserId" },
                values: new object[,]
                {
                    { 1, "Possimus necessitatibus et.", "Amet adipisci.", 0, 5, 1, 2, 31 },
                    { 2, "Nihil omnis temporibus occaecati quas qui veniam velit.", "Consectetur et.", 0, 4, 1, 6, 44 },
                    { 3, "In et dolor sit dolor culpa aspernatur.", "Repellendus repellendus.", 1, 1, 1, 6, 46 },
                    { 4, "Corporis architecto ut quae eveniet.", "Veniam sunt.", 1, 1, 0, 6, 7 },
                    { 5, "Ipsam omnis similique.", "Animi repudiandae.", 1, 4, 2, 1, 43 },
                    { 6, "Blanditiis et et.", "Quo et.", 0, 4, 2, 4, 50 },
                    { 7, "Dolorem assumenda alias dolorum dignissimos laudantium eum.", "Quasi ea.", 2, 5, 1, 6, 6 },
                    { 8, "Quae voluptas repellendus at sint accusamus.", "Et autem.", 0, 4, 0, 5, 33 },
                    { 9, "Vel soluta et aut sit.", "Est consequatur.", 1, 5, 1, 1, 42 },
                    { 10, "Atque corrupti adipisci suscipit aperiam nesciunt quidem sed vel illo.", "Est animi.", 2, 4, 1, 5, 11 },
                    { 11, "Dolor quia hic nam ullam natus accusamus consequatur.", "Qui voluptatem.", 0, 1, 1, 4, 13 },
                    { 12, "Omnis molestiae et.", "Itaque impedit.", 0, 2, 0, 2, 44 },
                    { 13, "Vero eos libero tenetur iure.", "Nostrum enim.", 2, 2, 0, 2, 37 },
                    { 14, "Eum dolores odio recusandae voluptatibus vel provident laboriosam consequatur laborum.", "Velit consequatur.", 0, 1, 2, 4, 39 },
                    { 15, "Consequatur eaque unde consequuntur qui.", "Reiciendis voluptatem.", 2, 4, 0, 6, 9 },
                    { 16, "Perspiciatis sint quia error totam perferendis.", "Eum dolorem.", 0, 1, 1, 4, 49 },
                    { 17, "Velit voluptate est quia minima officia omnis.", "Alias consequatur.", 0, 2, 0, 6, 14 },
                    { 18, "Asperiores laborum a dolor.", "Alias et.", 0, 3, 0, 1, 42 },
                    { 19, "Iste alias eveniet aut qui.", "Ex et.", 1, 4, 2, 2, 46 },
                    { 20, "Possimus dolorum quisquam quaerat quisquam eius beatae.", "Itaque facilis.", 2, 4, 2, 5, 17 },
                    { 21, "Consectetur autem voluptates sunt qui.", "Omnis omnis.", 1, 4, 0, 4, 45 },
                    { 22, "Ipsam explicabo tenetur omnis sed possimus enim.", "Optio accusantium.", 1, 4, 1, 6, 10 },
                    { 23, "Repellat omnis quia temporibus odio sint asperiores officiis nobis eum.", "Distinctio quia.", 0, 2, 2, 2, 4 },
                    { 24, "Possimus sequi mollitia laborum.", "Quo autem.", 0, 4, 1, 6, 42 },
                    { 25, "Sapiente neque similique ut delectus qui unde vitae.", "Voluptate et.", 0, 4, 0, 4, 36 },
                    { 26, "At consequatur natus quidem.", "Dolorum est.", 1, 4, 1, 6, 30 },
                    { 27, "Est nihil nihil quia.", "Sunt sequi.", 1, 1, 0, 4, 4 },
                    { 28, "Laudantium tempore ex.", "Necessitatibus esse.", 2, 2, 1, 1, 10 },
                    { 29, "Sapiente libero pariatur odit repellat.", "Molestias hic.", 2, 1, 1, 5, 32 },
                    { 30, "Voluptatem nisi nulla et.", "Provident sit.", 2, 1, 1, 5, 26 },
                    { 31, "Maxime culpa rerum deleniti.", "Quisquam quia.", 0, 5, 2, 6, 48 },
                    { 32, "Modi laudantium optio a.", "Ea placeat.", 2, 2, 0, 2, 44 },
                    { 33, "Magnam nemo asperiores est et quasi itaque.", "Velit natus.", 0, 5, 1, 5, 42 },
                    { 34, "Necessitatibus id iure.", "Vero aliquam.", 2, 2, 2, 1, 39 },
                    { 35, "Explicabo animi rerum laboriosam deleniti consequuntur aliquid doloribus.", "At quod.", 2, 3, 0, 1, 47 },
                    { 36, "Eum voluptates ab qui aut voluptates earum.", "Rerum facilis.", 1, 5, 0, 2, 46 },
                    { 37, "Architecto doloremque facere maxime et eum aliquam corporis qui.", "Excepturi veniam.", 2, 1, 0, 2, 38 },
                    { 38, "Est atque quae similique perspiciatis et blanditiis voluptatem dignissimos.", "Beatae et.", 2, 1, 2, 6, 34 },
                    { 39, "Earum tenetur et qui commodi impedit consequatur.", "Aut et.", 1, 4, 1, 4, 16 },
                    { 40, "Et iusto a quaerat minima quibusdam.", "Reprehenderit provident.", 0, 1, 0, 1, 50 },
                    { 41, "Ut voluptas odio.", "Eos quisquam.", 1, 1, 0, 6, 21 },
                    { 42, "Excepturi iure officiis voluptas velit provident molestias tempora.", "Aut ad.", 0, 2, 1, 5, 42 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Description", "Name", "Priority", "ProjectId", "Status", "StoryPoints", "UserId" },
                values: new object[,]
                {
                    { 43, "Odit autem ut repellendus sunt quia ut voluptate error.", "Enim error.", 2, 2, 1, 3, 25 },
                    { 44, "Aut voluptatem odio velit vitae quibusdam quasi voluptatem laudantium.", "Et reiciendis.", 2, 4, 1, 4, 35 },
                    { 45, "Veniam enim pariatur est est.", "Non molestias.", 0, 4, 0, 3, 44 },
                    { 46, "Rerum velit et nostrum et.", "Voluptas eaque.", 2, 2, 0, 4, 29 },
                    { 47, "Itaque eaque repudiandae et.", "Quos eius.", 1, 3, 1, 4, 23 },
                    { 48, "Sunt quia eum deleniti.", "Qui maxime.", 0, 4, 2, 1, 43 },
                    { 49, "Perspiciatis quia quidem consectetur commodi itaque occaecati animi accusantium.", "Fugiat ab.", 2, 2, 2, 4, 14 },
                    { 50, "Et accusamus dignissimos molestiae cum sed.", "Iste repellat.", 1, 3, 1, 6, 21 },
                    { 51, "Nisi qui in minima nulla non possimus.", "Exercitationem similique.", 0, 4, 0, 3, 35 },
                    { 52, "Non accusamus cum accusamus aut facilis.", "Quasi aut.", 0, 4, 0, 6, 23 },
                    { 53, "Dolorum quae iusto temporibus qui.", "Illo perferendis.", 2, 4, 2, 2, 13 },
                    { 54, "Odio magni voluptas iste non consequatur nemo.", "Ex id.", 0, 1, 2, 5, 12 },
                    { 55, "Alias voluptas eos fugit corporis ut dolorum omnis soluta.", "Atque ratione.", 2, 3, 2, 6, 38 },
                    { 56, "Cum amet architecto nulla iure sint.", "Velit perspiciatis.", 2, 1, 1, 4, 39 },
                    { 57, "Aperiam veniam et voluptatibus officiis omnis ut.", "Omnis eligendi.", 2, 1, 2, 1, 30 },
                    { 58, "Eos optio magnam et accusantium quo totam minus.", "Quia repellendus.", 0, 3, 2, 3, 28 },
                    { 59, "Et cumque quis minus illum neque repellendus qui repellendus molestiae.", "Eius dolore.", 0, 4, 0, 2, 50 },
                    { 60, "Inventore accusamus quo explicabo officia sit illo.", "Quo qui.", 1, 4, 2, 4, 34 },
                    { 61, "Labore totam impedit maxime et vel.", "Molestiae est.", 2, 1, 1, 2, 36 },
                    { 62, "Officia pariatur omnis omnis autem voluptas corrupti officiis aut.", "Voluptas consectetur.", 2, 2, 0, 1, 31 },
                    { 63, "Laudantium ducimus rerum molestias optio deserunt.", "Facere consequuntur.", 0, 3, 0, 1, 20 },
                    { 64, "Voluptatibus numquam aperiam earum aut quidem quisquam eum vel placeat.", "Et tempora.", 2, 3, 2, 5, 50 },
                    { 65, "Tenetur possimus veritatis nobis id consequatur.", "A ea.", 1, 2, 1, 2, 27 },
                    { 66, "Autem non quo cupiditate in exercitationem non sunt.", "Quia eaque.", 2, 2, 0, 2, 37 },
                    { 67, "Sed placeat nesciunt culpa sint nobis magnam aut.", "Distinctio minus.", 2, 4, 1, 4, 15 },
                    { 68, "Optio dolores assumenda qui quibusdam.", "At dolore.", 0, 3, 1, 1, 28 },
                    { 69, "Minima id dolorum.", "Perspiciatis exercitationem.", 1, 4, 2, 3, 33 },
                    { 70, "Quaerat occaecati nulla placeat corporis necessitatibus voluptatem.", "Rerum ut.", 2, 5, 0, 3, 42 },
                    { 71, "Possimus reiciendis possimus excepturi blanditiis quia qui ut id explicabo.", "Et nisi.", 2, 3, 2, 6, 43 },
                    { 72, "Aspernatur porro ut voluptatibus voluptate.", "Dolorum deleniti.", 1, 3, 0, 5, 34 },
                    { 73, "Laboriosam necessitatibus voluptatum assumenda.", "Voluptas corporis.", 1, 2, 2, 2, 12 },
                    { 74, "Fugiat natus repellat aut omnis.", "Enim sit.", 0, 1, 1, 4, 4 },
                    { 75, "Ratione sint qui dolorem sit assumenda.", "Repellat ut.", 2, 2, 1, 3, 10 },
                    { 76, "Accusantium nam sequi quasi voluptas aut.", "Modi tenetur.", 1, 1, 1, 2, 48 },
                    { 77, "Beatae quia minima.", "Quo occaecati.", 1, 1, 2, 3, 6 },
                    { 78, "Dicta quasi id in voluptate modi repellendus est et.", "Est nisi.", 2, 5, 2, 1, 31 },
                    { 79, "Accusantium ut ab aut et aut soluta nihil.", "Et aperiam.", 0, 4, 0, 6, 8 },
                    { 80, "Debitis a velit possimus totam.", "Iste est.", 2, 5, 2, 3, 36 },
                    { 81, "Rerum temporibus repudiandae est sunt eaque architecto odit ut.", "Ut ipsa.", 1, 2, 1, 2, 49 },
                    { 82, "Totam eligendi vel.", "Enim velit.", 2, 4, 1, 2, 21 },
                    { 83, "Delectus quia voluptates deserunt et aut praesentium sunt necessitatibus.", "Ut quia.", 0, 1, 0, 4, 41 },
                    { 84, "Quas consequuntur consequatur voluptas dignissimos consequatur.", "Fuga nobis.", 2, 2, 0, 5, 10 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Description", "Name", "Priority", "ProjectId", "Status", "StoryPoints", "UserId" },
                values: new object[,]
                {
                    { 85, "Est accusamus sit sint quod occaecati vel.", "Ut doloribus.", 0, 2, 2, 2, 36 },
                    { 86, "Minima eum harum similique quidem nam.", "Fuga at.", 2, 1, 1, 6, 20 },
                    { 87, "Suscipit explicabo provident.", "Odio et.", 2, 2, 1, 6, 28 },
                    { 88, "Sed molestiae labore qui iusto dolor dolor et consequatur.", "Magni quo.", 0, 1, 1, 1, 16 },
                    { 89, "Mollitia repellat blanditiis animi maiores ut.", "Quam quia.", 2, 1, 1, 6, 38 },
                    { 90, "Laborum veritatis vero quis doloremque.", "Fuga aut.", 2, 1, 0, 1, 7 },
                    { 91, "Ipsam dolores ipsam est similique id maiores vero omnis.", "Et eum.", 1, 1, 2, 1, 44 },
                    { 92, "Omnis et voluptatem consequuntur perspiciatis quia laboriosam dolor consectetur minima.", "Aut illo.", 1, 5, 1, 1, 36 },
                    { 93, "Nesciunt aut placeat officia.", "Accusantium fuga.", 2, 4, 2, 6, 8 },
                    { 94, "Nemo error et sit.", "Incidunt perferendis.", 2, 5, 1, 5, 13 },
                    { 95, "Et est quo non error et distinctio est.", "Autem incidunt.", 1, 2, 2, 1, 10 },
                    { 96, "Necessitatibus quo nemo.", "Placeat quo.", 1, 5, 1, 6, 21 },
                    { 97, "Quia autem et unde omnis ea eos voluptas deserunt et.", "Ad voluptatem.", 0, 5, 1, 3, 21 },
                    { 98, "Magnam molestias beatae voluptas.", "Quia earum.", 0, 4, 2, 5, 22 },
                    { 99, "Non sequi et labore iure dicta quo nihil.", "Voluptatibus non.", 2, 4, 1, 4, 16 },
                    { 100, "Quisquam illum illum iste atque dolorem.", "Quo ut.", 1, 2, 2, 6, 12 },
                    { 101, "Sunt rerum cumque similique dolorem et ducimus repellendus.", "Minus vero.", 0, 3, 2, 4, 21 },
                    { 102, "Aut sit consequatur id incidunt hic.", "Ea tenetur.", 2, 4, 0, 1, 7 },
                    { 103, "Minima eaque neque qui voluptate distinctio.", "Ut accusantium.", 0, 1, 0, 3, 49 },
                    { 104, "Qui voluptatem recusandae et delectus ea voluptas in.", "Id hic.", 1, 4, 0, 3, 29 },
                    { 105, "Est autem quo rerum voluptatem.", "Dolorem illum.", 1, 4, 2, 5, 36 },
                    { 106, "Nesciunt rerum quis impedit aut aut fugit qui.", "Molestiae reiciendis.", 1, 4, 2, 2, 17 },
                    { 107, "Ut omnis facere quibusdam praesentium id beatae et voluptatem.", "Odit necessitatibus.", 0, 2, 1, 1, 43 },
                    { 108, "Id aut qui eum cupiditate non placeat.", "Laudantium dolor.", 1, 4, 2, 1, 21 },
                    { 109, "Libero unde ad eveniet quia et velit.", "Harum dicta.", 2, 4, 1, 1, 21 },
                    { 110, "Enim voluptatibus rerum nesciunt hic ut.", "Ut quia.", 2, 3, 1, 1, 20 },
                    { 111, "Delectus sit optio ipsa ad reiciendis nihil ut nihil.", "Adipisci et.", 0, 3, 1, 5, 7 },
                    { 112, "Vero rerum nisi quos ipsa.", "Ad modi.", 0, 1, 0, 1, 10 },
                    { 113, "Molestias deleniti et velit laudantium sapiente beatae iure assumenda.", "Commodi commodi.", 1, 5, 0, 3, 30 },
                    { 114, "Aut rerum itaque voluptatem voluptate maxime sequi nesciunt.", "Cum velit.", 1, 3, 1, 6, 4 },
                    { 115, "Impedit velit ut sed reiciendis in sunt.", "Id dolorum.", 0, 3, 2, 6, 23 },
                    { 116, "Aliquam qui beatae voluptatem odit sint.", "Expedita nihil.", 2, 1, 1, 6, 17 },
                    { 117, "Aliquid dolorem est laborum.", "Voluptatem sed.", 0, 1, 1, 3, 1 },
                    { 118, "Facere fuga deserunt maiores recusandae rerum.", "Vel cum.", 1, 5, 0, 5, 29 },
                    { 119, "Sint rerum nam placeat veniam.", "Facere et.", 0, 3, 1, 2, 35 },
                    { 120, "Voluptatem in aut accusamus alias omnis.", "Tempore et.", 1, 3, 1, 1, 15 },
                    { 121, "Cumque ratione velit necessitatibus autem in magnam quia.", "Sunt tempora.", 1, 3, 2, 2, 20 },
                    { 122, "Aut nisi dignissimos rerum sit.", "Qui commodi.", 2, 1, 1, 6, 45 },
                    { 123, "Aut consequuntur deleniti.", "Consequatur laborum.", 1, 2, 2, 1, 42 },
                    { 124, "Ducimus beatae omnis ducimus ea quae.", "Et sed.", 1, 3, 0, 6, 50 },
                    { 125, "Modi amet iste corrupti est similique.", "Exercitationem voluptatibus.", 0, 1, 1, 5, 10 },
                    { 126, "Laboriosam quaerat ut.", "Dolore repellendus.", 0, 3, 0, 5, 20 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Description", "Name", "Priority", "ProjectId", "Status", "StoryPoints", "UserId" },
                values: new object[,]
                {
                    { 127, "Perferendis quidem et aperiam et aut.", "Sint voluptas.", 2, 5, 1, 3, 5 },
                    { 128, "Est accusantium fuga sunt sunt exercitationem voluptatem nam minus quas.", "Vel alias.", 2, 4, 1, 1, 22 },
                    { 129, "Sit illo magni.", "Deserunt ut.", 1, 2, 1, 6, 50 },
                    { 130, "Exercitationem nesciunt veniam hic nemo est et.", "Dolorem ab.", 2, 4, 2, 2, 14 },
                    { 131, "Ratione aliquid error ullam et pariatur eaque accusamus excepturi.", "Magnam autem.", 0, 3, 2, 5, 26 },
                    { 132, "Culpa corrupti mollitia.", "Necessitatibus rerum.", 0, 5, 0, 1, 42 },
                    { 133, "Quod et omnis eos quod.", "Quisquam aut.", 0, 4, 2, 3, 23 },
                    { 134, "Ratione deserunt rerum.", "Dolor placeat.", 1, 1, 2, 6, 19 },
                    { 135, "Enim animi quia voluptatem.", "Ducimus dolores.", 1, 4, 2, 2, 23 },
                    { 136, "Repellat ea doloremque tenetur placeat.", "Eaque nihil.", 1, 5, 1, 2, 22 },
                    { 137, "Quas fugiat et quae eligendi aliquid pariatur.", "Inventore harum.", 0, 3, 0, 3, 14 },
                    { 138, "Sit repellendus doloribus enim.", "Corrupti consequatur.", 2, 5, 2, 5, 15 },
                    { 139, "Excepturi necessitatibus illo adipisci omnis et tempora dolor.", "Voluptas neque.", 2, 3, 2, 5, 38 },
                    { 140, "Quam temporibus cumque unde velit dolores officiis.", "Inventore voluptatem.", 1, 1, 1, 2, 10 },
                    { 141, "Eum iusto laborum.", "Qui repellendus.", 1, 3, 0, 3, 29 },
                    { 142, "Quis labore quo sint et perspiciatis sed.", "Animi ab.", 0, 4, 2, 4, 20 },
                    { 143, "Sed quaerat harum recusandae sapiente consequatur dolor nihil minus.", "Ratione possimus.", 0, 2, 1, 1, 41 },
                    { 144, "Ad excepturi et velit cumque accusantium est.", "Similique minima.", 2, 2, 2, 1, 17 },
                    { 145, "Est qui aut sit.", "Ipsum ad.", 0, 1, 0, 1, 39 },
                    { 146, "Esse adipisci illo et enim architecto vel et excepturi.", "Aut nisi.", 1, 2, 1, 5, 12 },
                    { 147, "Qui placeat praesentium sapiente.", "Nihil consequatur.", 2, 5, 1, 4, 37 },
                    { 148, "Omnis voluptatibus distinctio voluptate quia sint placeat.", "Odio eum.", 2, 1, 0, 1, 31 },
                    { 149, "Id delectus possimus aliquid.", "Vitae ut.", 2, 4, 2, 6, 6 },
                    { 150, "Est quos molestias.", "Officia tempore.", 0, 5, 2, 4, 9 },
                    { 151, "Qui aut qui accusamus.", "Et quibusdam.", 0, 1, 1, 2, 41 },
                    { 152, "Et tempora aut facilis tenetur fugiat nesciunt.", "Quo a.", 2, 4, 0, 4, 43 },
                    { 153, "Omnis ipsa minus numquam odit labore harum beatae.", "Cumque perferendis.", 2, 5, 1, 4, 46 },
                    { 154, "Alias praesentium est et veritatis.", "Ut nesciunt.", 0, 2, 2, 5, 13 },
                    { 155, "Quasi sed quia inventore dolores porro optio accusantium sint est.", "At ab.", 0, 3, 2, 1, 15 },
                    { 156, "Reiciendis similique veritatis rerum.", "Doloribus autem.", 1, 2, 0, 3, 8 },
                    { 157, "Pariatur officia aliquam alias mollitia aut.", "In non.", 2, 4, 0, 4, 36 },
                    { 158, "Ducimus aliquam perferendis et ea et natus.", "Molestias quia.", 1, 5, 1, 6, 33 },
                    { 159, "Optio sit voluptatum ea qui praesentium sit doloremque.", "Consequatur pariatur.", 0, 5, 0, 1, 50 },
                    { 160, "Illo aut repudiandae voluptas vel distinctio.", "Blanditiis eos.", 1, 2, 0, 4, 49 },
                    { 161, "Labore corrupti est.", "Inventore consequatur.", 1, 2, 1, 5, 4 },
                    { 162, "Ab sit enim in laudantium.", "Et et.", 1, 4, 0, 2, 8 },
                    { 163, "Veritatis ipsam laboriosam.", "Quia nihil.", 1, 3, 0, 6, 28 },
                    { 164, "Quas aperiam dolores expedita et aperiam voluptas.", "Aut odit.", 2, 1, 0, 5, 2 },
                    { 165, "Itaque enim dolores non.", "Consectetur praesentium.", 0, 5, 0, 6, 19 },
                    { 166, "Beatae sint distinctio qui dolor architecto.", "Itaque sed.", 2, 5, 0, 3, 38 },
                    { 167, "Aut quam qui ratione architecto sint et aut.", "Similique quasi.", 2, 2, 0, 2, 50 },
                    { 168, "Voluptates nesciunt tempora est voluptatum molestiae dolor deserunt.", "Rerum nihil.", 0, 1, 0, 1, 15 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "Description", "Name", "Priority", "ProjectId", "Status", "StoryPoints", "UserId" },
                values: new object[,]
                {
                    { 169, "Maxime ad delectus veniam ea veritatis mollitia.", "Neque culpa.", 1, 2, 1, 6, 3 },
                    { 170, "Autem fugit dolor amet.", "Ipsam et.", 2, 2, 2, 5, 40 },
                    { 171, "Est et voluptatum corporis.", "Non at.", 0, 1, 1, 5, 23 },
                    { 172, "Provident omnis dolore et voluptas ut reiciendis inventore tempore iusto.", "Et quasi.", 0, 3, 2, 3, 38 },
                    { 173, "Maiores quidem nostrum minima saepe autem fuga praesentium.", "Ratione soluta.", 2, 4, 0, 5, 18 },
                    { 174, "Et molestias pariatur eum at sed omnis non qui itaque.", "Quae est.", 1, 2, 1, 3, 30 },
                    { 175, "Cumque magni et eum labore fugit est.", "Aut eos.", 2, 2, 1, 5, 1 },
                    { 176, "Reiciendis harum consequatur ea quo veniam sequi saepe.", "Necessitatibus libero.", 0, 1, 1, 3, 45 },
                    { 177, "Id hic omnis voluptas rerum quisquam unde rem.", "Impedit earum.", 1, 2, 1, 6, 17 },
                    { 178, "Nobis distinctio ratione ut.", "Possimus repudiandae.", 1, 5, 2, 2, 48 },
                    { 179, "Eos in non quis ut voluptatem in perspiciatis id.", "Et ipsam.", 2, 5, 0, 6, 38 },
                    { 180, "Quia esse eum voluptatem tenetur at perferendis.", "Nesciunt minus.", 2, 1, 1, 5, 31 },
                    { 181, "Nisi ut qui provident omnis.", "Veritatis placeat.", 2, 2, 2, 4, 42 },
                    { 182, "Quibusdam autem reprehenderit molestiae.", "Quos ex.", 0, 2, 2, 1, 6 },
                    { 183, "Aut quaerat quas praesentium.", "Quibusdam omnis.", 2, 1, 0, 6, 1 },
                    { 184, "Fugit consequatur ullam sequi beatae beatae.", "Neque dolorum.", 0, 4, 0, 5, 6 },
                    { 185, "Eos et sint tenetur.", "Doloremque quam.", 0, 5, 2, 2, 32 },
                    { 186, "Dolor et sapiente dolores omnis veritatis reprehenderit cum.", "Sequi voluptatem.", 1, 4, 2, 2, 26 },
                    { 187, "Aliquid repellendus itaque qui animi vero perspiciatis accusantium.", "Minima consectetur.", 1, 4, 0, 1, 26 },
                    { 188, "Et debitis hic iusto dolores harum enim animi amet perferendis.", "Voluptatum et.", 0, 4, 1, 4, 28 },
                    { 189, "Nisi hic est libero nemo sint nam odio consequatur consectetur.", "Consequatur error.", 0, 1, 1, 4, 9 },
                    { 190, "Sed sunt sint quisquam quas necessitatibus.", "Atque nostrum.", 2, 3, 1, 4, 20 },
                    { 191, "Voluptatem voluptatem quasi sint fugiat.", "Pariatur accusamus.", 0, 3, 0, 5, 41 },
                    { 192, "Eius quisquam qui.", "Velit praesentium.", 2, 3, 2, 6, 45 },
                    { 193, "Repellendus nobis eum quidem ea cupiditate vitae ut voluptatum.", "Autem incidunt.", 0, 5, 2, 3, 40 },
                    { 194, "Rem modi et vero ut.", "Doloremque non.", 2, 1, 0, 6, 7 },
                    { 195, "Magnam non similique.", "Optio quaerat.", 1, 2, 2, 4, 42 },
                    { 196, "Tempora rem id quisquam officia qui autem.", "Qui nihil.", 0, 4, 2, 2, 39 },
                    { 197, "Ut commodi optio.", "Eveniet harum.", 1, 4, 0, 4, 31 },
                    { 198, "Distinctio cupiditate est eum nihil.", "Qui animi.", 0, 5, 0, 5, 1 },
                    { 199, "Possimus tempora maxime.", "Quae voluptatem.", 1, 4, 1, 2, 50 },
                    { 200, "Quam fugit quasi.", "Repellendus quos.", 1, 2, 2, 6, 17 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employees_TeamId",
                table: "Employees",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_UserId",
                table: "Tasks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Teams_ProjectId",
                table: "Teams",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "Projects");
        }
    }
}
