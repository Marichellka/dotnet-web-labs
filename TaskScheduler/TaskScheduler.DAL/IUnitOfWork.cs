﻿using TaskScheduler.DAL.Models;
using TaskScheduler.DAL.Repositories;

namespace TaskScheduler.DAL;

public interface IUnitOfWork
{
    IRepository<Project> ProjectRepository { get; }
    IRepository<Story> StoryRepository { get; }
    IRepository<Team> TeamRepository { get; }
    IRepository<User> UserRepository { get; }
    Task<int> SaveChangesAsync();
}