﻿using TaskScheduler.DAL.Contexts;
using TaskScheduler.DAL.Models;
using TaskScheduler.DAL.Repositories;

namespace TaskScheduler.DAL;

public class UnitOfWork:IUnitOfWork
{
    private readonly AppDbContext _context;

    public UnitOfWork(AppDbContext context)
    {
        _context = context;
        ProjectRepository = new Repository<Project>(_context);
        StoryRepository = new Repository<Story>(_context);
        TeamRepository = new Repository<Team>(_context);
        UserRepository = new Repository<User>(_context);
    }

    public IRepository<Project> ProjectRepository { get; }
    public IRepository<Story> StoryRepository { get; }
    public IRepository<Team> TeamRepository { get; }
    public IRepository<User> UserRepository { get; }
    
    public Task<int> SaveChangesAsync()
    {
       return _context.SaveChangesAsync();
    }
}